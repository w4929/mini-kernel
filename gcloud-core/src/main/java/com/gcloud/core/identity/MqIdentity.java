package com.gcloud.core.identity;

import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.service.ServiceName;
import com.gcloud.header.identity.api.GetApiMsg;
import com.gcloud.header.identity.api.GetApiReplyMsg;
import com.gcloud.header.identity.ldap.GetLdapConfMsg;
import com.gcloud.header.identity.ldap.GetLdapConfReplyMsg;
import com.gcloud.header.identity.role.ApiGetFunctionRightMsg;
import com.gcloud.header.identity.role.ApiGetFunctionRightReplyMsg;
import com.gcloud.header.identity.role.DescribeUncheckApiMsg;
import com.gcloud.header.identity.role.DescribeUncheckApiReplyMsg;
import com.gcloud.header.identity.role.model.CheckRightMsg;
import com.gcloud.header.identity.role.model.CheckRightReplyMsg;
import com.gcloud.header.identity.user.CheckTokenMsg;
import com.gcloud.header.identity.user.CheckTokenReplyMsg;
import com.gcloud.header.identity.user.GetUserByAccessKeyMsg;
import com.gcloud.header.identity.user.GetUserByAccessKeyReplyMsg;
import com.gcloud.header.identity.user.GetUserMsg;
import com.gcloud.header.identity.user.GetUserReplyMsg;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Service
public class MqIdentity implements IApiIdentity {
	@Autowired
	MessageBus bus;
	@Autowired
	ServiceName serviceName;
	public TokenUser checkToken(String token) {
		CheckTokenMsg msg=new CheckTokenMsg();
		msg.setToken(token);
		msg.setServiceId(serviceName.getIdentity());
		CheckTokenReplyMsg reply=bus.call(msg,CheckTokenReplyMsg.class);
		if(reply.getUserInfo()==null)
			return null;
		TokenUser tokenUser=new TokenUser();
		tokenUser.setExpressTime(reply.getExpressTime());
//		tokenUser.setUserId(reply.getUserId());
//		tokenUser.setLoginName(reply.getLoginName());
		tokenUser.setUserInfo(reply.getUserInfo());
		CacheContainer.getInstance().put(CacheType.TOKEN_USER,token,tokenUser);
		return tokenUser;
	}
	
	public SignUser getUserByAccessKey(String accessKey) {
		GetUserByAccessKeyMsg msg=new GetUserByAccessKeyMsg();
		msg.setAccessKey(accessKey);
		msg.setServiceId(serviceName.getIdentity());
		GetUserByAccessKeyReplyMsg reply=  bus.call(msg,GetUserByAccessKeyReplyMsg.class);
		if(reply.getUserId()==null)
			return null;
		SignUser signUser=new SignUser();
		signUser.setSecretKey(reply.getSecretKey());
		signUser.setUserId(reply.getUserId());
		signUser.setUserInfo(reply.getUserInfo());
		//CacheContainer.getInstance().put(CacheType.SIGN_USER,accessKey,signUser);//暂时去掉缓存
		return signUser;
	}
	
	public GetUserReplyMsg getUserById(String userId) {
		GetUserMsg msg=new GetUserMsg();
		msg.setId(userId);
		msg.setServiceId(serviceName.getIdentity());
		GetUserReplyMsg reply=bus.call(msg,GetUserReplyMsg.class);
		return reply;
	}

	/*@Override
	public ApiGetRoleRightReplyMsg getRoleRight(String roleId, String regionId) {
		ApiGetRoleRightMsg msg=new ApiGetRoleRightMsg();
		msg.setRoleId(roleId);
		msg.setParamRegionId(regionId);
		msg.setServiceId(serviceName.getIdentity());
		ApiGetRoleRightReplyMsg reply=bus.call(msg, ApiGetRoleRightReplyMsg.class);
		return reply;
	}

	@Override
	public ApiDetailRoleReplyMsg getRoleDetail(String roleId) {
		ApiDetailRoleMsg msg=new ApiDetailRoleMsg();
		msg.setRoleId(roleId);
		msg.setServiceId(serviceName.getIdentity());
		ApiDetailRoleReplyMsg reply=bus.call(msg, ApiDetailRoleReplyMsg.class);
		return reply;
	}*/
	
	/*@Override
	public ApiGetFunctionRightReplyMsg getFunctionRight(String path) {
		ApiGetFunctionRightMsg msg=new ApiGetFunctionRightMsg();
		msg.setPath(path);
		msg.setServiceId(serviceName.getIdentity());
		ApiGetFunctionRightReplyMsg reply=bus.call(msg, ApiGetFunctionRightReplyMsg.class);
		return reply;
	}*/

	@Override
	public DescribeUncheckApiReplyMsg getUncheckApi(String type) {
		DescribeUncheckApiMsg msg = new DescribeUncheckApiMsg();
		msg.setType(type);
		msg.setServiceId(serviceName.getIdentity());
		DescribeUncheckApiReplyMsg reply = bus.call(msg, DescribeUncheckApiReplyMsg.class);
		return reply;
	}
	
	public GetLdapConfReplyMsg getLdapConf(String domain) {
		GetLdapConfMsg msg=new GetLdapConfMsg();
		//msg.setDomainId(domainId);
		msg.setDomain(domain);
		msg.setServiceId(serviceName.getIdentity());
		GetLdapConfReplyMsg reply=bus.call(msg,GetLdapConfReplyMsg.class);
		return reply;
	}

	@Override
	public CheckRightReplyMsg checkRight(String roleId, String regionId, String funcPath) {
		CheckRightMsg msg = new CheckRightMsg();
		msg.setRoleId(roleId);
		msg.setParamRegionId(regionId);
		msg.setFuncPath(funcPath);
		msg.setServiceId(serviceName.getIdentity());
		CheckRightReplyMsg reply=bus.call(msg,CheckRightReplyMsg.class);
		if(StringUtils.isNotBlank(reply.getErrorMsg())) {
			throw new GCloudException(reply.getErrorMsg());
		}
		return reply;
	}

	@Override
	public GetApiReplyMsg getApi(String path) {
		GetApiMsg msg=new GetApiMsg();
		msg.setPath(path);
		msg.setServiceId(serviceName.getIdentity());
		GetApiReplyMsg reply=bus.call(msg, GetApiReplyMsg.class);
		return reply;
	}
}