package com.gcloud.core.util;

import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.service.SpringUtil;
import com.gcloud.header.GMessage;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.core.env.Environment;
import org.springframework.util.SerializationUtils;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Slf4j
public class MessageUtil {

    private static String controllerServiceId;
    private static Long messageTimeout;

    public static String computeServiceId(String hostname){
        return String.format("compute-%s", hostname);
    }

    public static String networkServiceId(String hostname){
        return String.format("network-%s", hostname);
    }

    public static String storageServiceId(String hostname){
        return String.format("storage-%s", hostname);
    }
    
    public static String imageServiceId(String hostname){
        return String.format("image-%s", hostname);
    }

    public static String controllerServiceId(){
        return getControllerServiceId();
    }

    private static String getControllerServiceId(){

        if(controllerServiceId == null){
            Environment env = SpringUtil.getBean(Environment.class);
            controllerServiceId = env.getProperty("gcloud.service.controller");
        }
        return controllerServiceId;
    }


    public static GMessage ackAndGetMessage(String ackMode, Message msg, Channel channel){

        //ackMde传入，允许同�?个环境不同队列多种配�?
        if(ackMode != null && "manual".equals(ackMode)){
            try{
                channel.basicAck(msg.getMessageProperties().getDeliveryTag(), false);
            }catch (Exception ex){
                log.error("akc error", ex);
                return null;
            }
        }

        //手动反序列化，否则会无法ack�? 没有配置dead-letter，导致unacked�?直堆�?
        GMessage message = null;
        try{
            message = (GMessage) SerializationUtils.deserialize(msg.getBody());
        }catch (Exception ex){
            log.error("反序列化失败，ex=" + ex, ex);
            throw new GCloudException("::消息处理失败");
        }

        return message;
    }

    public static GMessage fromMessage(Object object){

        GMessage message = null;
        if(object instanceof GMessage){
            message = (GMessage) object;
        }else if(object instanceof byte[]){
            try{
                message = (GMessage) SerializationUtils.deserialize((byte[])object);
            }catch (Exception ex){
                log.error("反序列化失败，ex=" + ex, ex);
                throw new GCloudException("::消息处理失败");
            }
        }
        return message;
    }

}