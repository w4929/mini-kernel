package com.gcloud.core.cache.redis.lock.util;

import com.gcloud.core.cache.DistributedLock;
import com.gcloud.core.cache.redis.lock.RedisSpinLock;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.service.SpringUtil;
import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Slf4j
public class LockUtil {

	public static String spinLock(String lockName, long lockTimeout, long getLockTimeout){

        DistributedLock redisLock = SpringUtil.getApplicationContext().getBean(RedisSpinLock.class);
        return redisLock.getLock(lockName, lockTimeout, getLockTimeout);
    }

    public static String spinLock(String lockName, long lockTimeout, long getLockTimeout, String errorCode) throws GCloudException{

        String lockId;
        try{
            lockId = spinLock(lockName, lockTimeout, getLockTimeout);
        }catch (Exception ex){
            log.error(String.format("获取锁失�?,lock=%s", lockName), ex);
            throw new GCloudException(errorCode);
        }

        return lockId;
    }


    public static boolean releaseSpinLock(String lockName, String lockId){
    	DistributedLock redisLock = SpringUtil.getApplicationContext().getBean(RedisSpinLock.class);
    	boolean release = false;
        try{
            release = redisLock.releaseLock(lockName, lockId);
        }catch (Exception ex){
            log.error(String.format("获取锁失�?,lock=%s,lockId=%s", lockName, lockId), ex);
        }
        return release;
    }

}