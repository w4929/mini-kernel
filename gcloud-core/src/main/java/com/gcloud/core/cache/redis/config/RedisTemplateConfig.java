package com.gcloud.core.cache.redis.config;

import com.gcloud.core.cache.redis.template.GCloudRedisTemplate;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;

import java.net.UnknownHostException;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Configuration
@ConditionalOnExpression("${gcloud.redis.enable:false} == true")
public class RedisTemplateConfig {

    //改用StringRedisSerializer
    @Bean
    public GCloudRedisTemplate gcloudRedisTemplate(RedisConnectionFactory redisConnectionFactory) throws UnknownHostException {
        GCloudRedisTemplate template = new GCloudRedisTemplate();
        template.setConnectionFactory(redisConnectionFactory);
        return template;
    }


}