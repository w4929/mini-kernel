package com.gcloud.core.log.enums;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum LogResult {
	SUCCESS((byte)1, "成功"),
	FAIL((byte)2, "失败"),
	RUNNING((byte)3, "进行�?"),
	TIMEOUT((byte)4, "超时");
	
	private Byte result;
	private String resultCn;
	LogResult(Byte result, String resultCn)
	{
		this.result = result;
		this.resultCn = resultCn;
	}
	public Byte getResult() {
		return result;
	}
	public void setResult(Byte result) {
		this.result = result;
	}
	public String getResultCn() {
		return resultCn;
	}
	public void setResultCn(String resultCn) {
		this.resultCn = resultCn;
	}
	
}