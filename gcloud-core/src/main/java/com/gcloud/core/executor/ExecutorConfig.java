package com.gcloud.core.executor;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Configuration
@EnableAsync
@Slf4j
public class ExecutorConfig {
	@Bean
    public Executor asyncExecutor() {
	    log.info("start asyncExecutor");
	    ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
	    //配置核心线程�?
	    executor.setCorePoolSize(5);
	    //配置�?大线程数
	    executor.setMaxPoolSize(5);
	    //配置队列大小
	    executor.setQueueCapacity(99999);
	    //配置线程池中的线程的名称前缀
	    executor.setThreadNamePrefix("async-service-");
	
	    // rejection-policy：当pool已经达到max size的时候，如何处理新任�?
	    // CALLER_RUNS：不在新线程中执行任务，而是有调用�?�所在的线程来执�?
	    executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
	    //执行初始�?
	    executor.initialize();
	    return executor;
    }
}