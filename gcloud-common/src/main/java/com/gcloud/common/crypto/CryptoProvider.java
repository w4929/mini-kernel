package com.gcloud.common.crypto;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public interface CryptoProvider extends BaseSecurityProvider
{
	
	String generateId( String seed, String prefix );
	String generateQueryId();
    String generateSecretKey();
//  String generateHashedPassword( String password );
//  String generateSessionToken();


//	String getDigestBase64( String input, Digest hash );
//	String getFingerPrint( byte[] data );
//	String generateLinuxSaltedPassword( String password );
//	boolean verifyLinuxSaltedPassword( String clear, String hashed );
	  
}