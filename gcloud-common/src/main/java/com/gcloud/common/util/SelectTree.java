package com.gcloud.common.util;

import java.util.Set;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */



public interface SelectTree {
	/**
	 * 获得树ID
	 * 
	 * @return
	 */
	public Long getId();

	/**
	 * 获得父节�?
	 * 
	 * @return
	 */
	public SelectTree getTreeParent();

	/**
	 * 获得原节点名�?
	 * 
	 * @return
	 */
	public String getTreeName();

	/**
	 * 获得下拉列表树名�?
	 * 
	 * @return
	 */
	public String getSelectTree();

	/**
	 * 设置下拉列表树名�?
	 * 
	 * @param selectTree
	 */
	public void setSelectTree(String selectTree);

	/**
	 * 获得树的子节点�?�可以被处理，比如为null时，可以直接调用getChild()
	 * 
	 * @return
	 */
	public Set<? extends SelectTree> getTreeChild();

	/**
	 * 未处理的子节点�?�如果没有调用setTreeChild，则该方法返回null�?
	 * 
	 * @return
	 */
	public Set<? extends SelectTree> getTreeChildRaw();

	/**
	 * 设置树的子节�?
	 * 
	 * @param treeChild
	 */
	public void setTreeChild(Set<?> treeChild);
}