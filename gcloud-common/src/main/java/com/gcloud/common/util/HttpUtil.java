package com.gcloud.common.util;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class HttpUtil {
	public static void post(String urlStr){
		URL url;
		try {
			url = new URL(urlStr);
			url.openConnection();
		} catch (MalformedURLException e) {
			
			System.out.println(e.getMessage());
		} catch (IOException e) {
			
			System.out.println(e.getMessage());
		}
	}
	
	/** 
	* @Title testConnection 
	* @Description 测试url联�?��??
	* @param @param urlStr
	* @param @return 
	* @return boolean    返回类型 
	* @throws 
	*/
	public static boolean testConnection(String urlStr)
	{
		try
		{  
            URL url = new URL(urlStr);  
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();  
            /** 
             * public int getResponseCode()throws IOException 
             * �? HTTP 响应消息获取状�?�码�? 
             * 例如，就以下状�?�行来说�?  
             * HTTP/1.0 200 OK 
             * HTTP/1.0 401 Unauthorized 
             * 将分别返�? 200 �? 401�? 
             * 如果无法从响应中识别任何代码（即响应不是有效�? HTTP），则返�? -1�?  
             *  
             * 返回 HTTP 状�?�码�? -1 
             */  
            int state = conn.getResponseCode();  
            if(state == 200)
            {  
            	return true;
            }  
            else
            {  
            	return false;
            }  
        }  
        catch(IOException e)
        {  
        	return false;
        }  
	}


	public static String url(String... names){
		return FileUtil.fileString(names);
	}

	public static String queryString(String url){
		String reg = "[^\\?]+\\?(.+)";
		Pattern p = Pattern.compile(reg);
		Matcher matcher = p.matcher(url);
		String result = null;
		if (matcher.find()) {
			result = matcher.group(1);
		}
		return result;
	}

}