package com.gcloud.common.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class RegexUtil {
	/**1-65535为合法的端口
	 * @param portStr
	 * @return
	 */
	public static boolean isValidPort(String portStr) {
		String regEx = "((^[1-9]\\d{0,3}$)|(^[1-5]\\d{4}$)|(^6[0-4]\\d{3}$)|(^65[0-4]\\d{2}$)|(^655[0-2]\\d$)|(^6553[0-5]$))";
	    Pattern pattern = Pattern.compile(regEx);
		Matcher matcher = pattern.matcher(portStr);
	    return matcher.matches();
	}
}