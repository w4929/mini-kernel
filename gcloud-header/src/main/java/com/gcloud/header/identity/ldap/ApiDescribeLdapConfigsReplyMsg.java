package com.gcloud.header.identity.ldap;

import java.util.List;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiDescribeLdapConfigsReplyMsg extends PageReplyMessage<LdapConfigItem>{
	@ApiModel(description="响类对象")
	private DescribeLdapConfigsResponse ldapConfigs;
	@Override
	public void setList(List<LdapConfigItem> list) {
		// TODO Auto-generated method stub
		ldapConfigs=new DescribeLdapConfigsResponse();
		ldapConfigs.setLdapConfig(list);
	}
	public DescribeLdapConfigsResponse getLdapConfigs() {
		return ldapConfigs;
	}
	public void setLdapConfigs(DescribeLdapConfigsResponse ldapConfigs) {
		this.ldapConfigs = ldapConfigs;
	}
	
}