package com.gcloud.header.identity.tenant;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.identity.model.DescribeTenantResponse;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DescribeTenantReplyMsg extends PageReplyMessage<TenantModel>{
	@ApiModel(description="租户列表")
	private DescribeTenantResponse tanants;
	
	@Override
	public void setList(List<TenantModel> list) {
		tanants = new DescribeTenantResponse();
		tanants.setTenant(list);
	}

	public DescribeTenantResponse getTanants() {
		return tanants;
	}

	public void setTanants(DescribeTenantResponse tanants) {
		this.tanants = tanants;
	}
}