package com.gcloud.header;

import java.util.List;

import com.gcloud.framework.db.PageResult;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public abstract class PageReplyMessage<E> extends ApiReplyMessage{
	private Integer pageSize;
	private Integer pageNumber;
	private Integer totalCount;
	
	public abstract void setList(List<E> list);
	
	public void init(PageResult<E> pageResult){
		this.pageSize=pageResult.getPageSize();
		this.pageNumber=pageResult.getPageNo();
		this.totalCount=pageResult.getTotalCount();
		setList(pageResult.getList());
	}
	
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public Integer getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}
	public Integer getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
}