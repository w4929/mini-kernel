package com.gcloud.header.storage.msg.node.volume.lvm.model;

import java.io.Serializable;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */



public class LvInfo implements Serializable{
	private LvStatus status;
	private String path;
	private float size;
	private String unit="GB";
	private String vg;
	private String lv;
	public LvInfo(){
		
	}
	public LvInfo(LvStatus status,String path,float size){
		this.status=status;
		this.path=path;
		this.size=size;
		spiltPath(path);
	}
	private void spiltPath(String path){
		String[] strs=path.split("/");
		int length=strs.length;
		if(length>2){
			this.vg=strs[length-2];
			this.lv=strs[length-1];
		}
	}
	public enum LvStatus{
		ACTIVE,inactive;
	}
	public LvStatus getStatus() {
		return status;
	}
	public void setStatus(LvStatus status) {
		this.status = status;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
		spiltPath(path);
	}
	public float getSize() {
		return size;
	}
	public void setSize(float size) {
		this.size = size;
	}
	public String getVg() {
		return vg;
	}
	public void setVg(String vg) {
		this.vg = vg;
	}
	public String getLv() {
		return lv;
	}
	public void setLv(String lv) {
		this.lv = lv;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
}