package com.gcloud.header.compute.enums;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum CloudDiskTargetDev {
	VDA(0, "vda"),
	VDB(1, "vdb"),
	VDC(2, "vdc"),
	VDD(3, "vdd"),
	VDE(4, "vde"),
	VDF(5, "vdf"),
	VDG(6, "vdg"),
	VDH(7, "vdh"),
	VDI(8, "vdi"),
	VDJ(9, "vdj");
	
	private Integer value;
	private String name;
	
	CloudDiskTargetDev (Integer value, String name) {
		this.value = value;
		this.name = name;
	}

	public Integer getValue() {
		return value;
	}
	
	public String getName(){
		return name;
	}
	
	public static String getDev(Integer index) {
		for(CloudDiskTargetDev dev:CloudDiskTargetDev.values()) {
			if(dev.getValue().equals(index)) {
				return dev.getName();
			}
		}
		return null;
	}
}