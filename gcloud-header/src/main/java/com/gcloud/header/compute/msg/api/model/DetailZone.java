package com.gcloud.header.compute.msg.api.model;

import java.io.Serializable;

import com.gcloud.header.api.ApiModel;
import com.gcloud.header.compute.msg.api.vm.zone.AvailableResource;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DetailZone implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "可用区ID")
	private String zoneId;
	@ApiModel(description = "可用区名�?")
    private String localName;
	@ApiModel(description = "可用区支持的资源")
    private AvailableResource availableResource;
	@ApiModel(description = "是否可用")
    private Boolean enabled;
	@ApiModel(description = "中文状�??")
    private String cnStatus;

    public String getZoneId() {
		return zoneId;
	}
	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}
	public String getLocalName() {
		return localName;
	}
	public void setLocalName(String localName) {
		this.localName = localName;
	}
	public AvailableResource getAvailableResource() {
		return availableResource;
	}
	public void setAvailableResource(AvailableResource availableResource) {
		this.availableResource = availableResource;
	}
	public Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	public String getCnStatus() {
		return cnStatus;
	}
	public void setCnStatus(String cnStatus) {
		this.cnStatus = cnStatus;
	}
}