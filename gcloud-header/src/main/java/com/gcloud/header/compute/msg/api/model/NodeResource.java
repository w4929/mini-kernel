package com.gcloud.header.compute.msg.api.model;

import java.io.Serializable;

import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class NodeResource implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModel(description = "剩余CPU")
    private int cpuAvail;
    @ApiModel(description = "总CPU")
    private int cpuTotal;
    @ApiModel(description = "已用CPU")
    private int cpuUsed;
    @ApiModel(description = "剩余内存,单位MB")
    private int memoryAvail;
    @ApiModel(description = "总内�?,单位MB")
    private int memoryTotal;
    @ApiModel(description = "已用内存,单位MB")
    private int memoryUsed;


    public int getCpuAvail() {
        return cpuAvail < 0 ? 0 : cpuAvail;
    }

    public void setCpuAvail(int cpuAvail) {
        this.cpuAvail = cpuAvail;
    }

    public int getCpuTotal() {
        return cpuTotal;
    }

    public void setCpuTotal(int cpuTotal) {
        this.cpuTotal = cpuTotal;
    }

    public int getCpuUsed() {
        return (cpuTotal - cpuAvail) < 0 ? 0 : (cpuTotal - cpuAvail);
    }

    public void setCpuUsed(int cpuUsed) {
        this.cpuUsed = cpuUsed;
    }

    public void addCpuTotal(int cpuTotal) {
        this.cpuTotal += cpuTotal;
    }

    public void addCpuAvail(int cpuAvail) {
        this.cpuAvail += cpuAvail;
    }


    public int getMemoryAvail() {
        return memoryAvail < 0 ? 0 : memoryAvail;
    }

    public void setMemoryAvail(int memoryAvail) {
        this.memoryAvail = memoryAvail;
    }

    public void addMemoryAvail(int memoryAvail) {
        this.memoryAvail += memoryAvail;
    }

    public int getMemoryTotal() {
        return memoryTotal;
    }

    public void setMemoryTotal(int memoryTotal) {
        this.memoryTotal = memoryTotal;
    }

    public void addMemoryTotal(int memoryTotal) {
        this.memoryTotal += memoryTotal;
    }

    public int getMemoryUsed() {
        return memoryTotal - memoryAvail < 0 ? 0 : (memoryTotal - memoryAvail);
    }

    public void setMemoryUsed(int memoryUsed) {
        this.memoryUsed = memoryUsed;
    }


}