package com.gcloud.header.network.msg.api.standard;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class StandardApiModifySecurityGroupAttributeMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;
	
	@NotBlank(message = "0040201")
	@ApiModel(description = "安全组ID", require = true)
	private String securityGroupId;
	@ApiModel(description = "安全组名�?")
	@Length(max = 255, message = "0040203::安全组名称长度不能大�?255")
	private String securityGroupName;
	@ApiModel(description = "描述")
	@Length(max = 255, message = "0040204::描述长度不能大于255")
	private String description;

	@Override
	public Class replyClazz() {
		return StandardApiModifySecurityGroupAttributeReplyMsg.class;
	}

	public String getSecurityGroupId() {
		return securityGroupId;
	}

	public void setSecurityGroupId(String securityGroupId) {
		this.securityGroupId = securityGroupId;
	}

	public String getSecurityGroupName() {
		return securityGroupName;
	}

	public void setSecurityGroupName(String securityGroupName) {
		this.securityGroupName = securityGroupName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}