package com.gcloud.header.network.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gcloud.framework.db.jdbc.annotation.TableField;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.network.model.standard.StandardNetworkInterfaceSet;

import java.io.Serializable;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class NetworkInterfaceSet extends StandardNetworkInterfaceSet implements Serializable{
	@ApiModel(description = "设备ID")
	@TableField("device_id")
	private String deviceId; // 弹�?�网卡当前关联的实例 ID
	@ApiModel(description = "实例类型")
	@TableField("device_owner")
	private String deviceType; // 弹�?�网卡当前关联的实例 ID
	@ApiModel(description = "浮动IP")
	private String floatingIpAddress;

	@JsonIgnore
	private String securityGroupIdsStr;

	public String getFloatingIpAddress() {
		return floatingIpAddress;
	}

	public void setFloatingIpAddress(String floatingIpAddress) {
		this.floatingIpAddress = floatingIpAddress;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getSecurityGroupIdsStr() {
		return securityGroupIdsStr;
	}

	public void setSecurityGroupIdsStr(String securityGroupIdsStr) {
		this.securityGroupIdsStr = securityGroupIdsStr;
	}
}