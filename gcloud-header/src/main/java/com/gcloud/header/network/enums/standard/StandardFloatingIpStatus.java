package com.gcloud.header.network.enums.standard;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.gcloud.header.network.enums.FloatingIpStatus;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum StandardFloatingIpStatus {

    AVAILABLE("Available", "可使�?", Arrays.asList(FloatingIpStatus.ACTIVE)),
    IN_USE("InUse", "已使�?", Arrays.asList(FloatingIpStatus.DOWN)),
    UNASSOCIATING("Unassociating", "解绑�?", null),
    ASSOCIATING("Associating", "绑定�?", null),
    DELETED("Deleted", "已删�?", null);

    StandardFloatingIpStatus(String value, String cnName, List<FloatingIpStatus> gcStatus) {
        this.value = value;
        this.cnName = cnName;
        this.gcStatus = gcStatus;
    }

    private String value;
    private String cnName;
    private List<FloatingIpStatus> gcStatus;

    public static String standardStatus(String gcStatusStr){

        if(StringUtils.isBlank(gcStatusStr)){
            return gcStatusStr;
        }

        for(StandardFloatingIpStatus status : StandardFloatingIpStatus.values()){
            if(status.gcStatus == null || status.getGcStatus().size() == 0){
                continue;
            }
            for(FloatingIpStatus gcStatus : status.getGcStatus()){
                if(gcStatus.value().equals(gcStatusStr)){
                    return status.getValue();
                }
            }
        }

        return null;
    }

    public String getValue() {
        return value;
    }

    public String getCnName() {
        return cnName;
    }

    public List<FloatingIpStatus> getGcStatus() {
        return gcStatus;
    }
}