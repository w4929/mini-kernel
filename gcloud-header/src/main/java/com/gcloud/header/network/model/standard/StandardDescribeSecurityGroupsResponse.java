package com.gcloud.header.network.model.standard;

import java.io.Serializable;
import java.util.List;

import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class StandardDescribeSecurityGroupsResponse implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "安全组信�?")
	private List<StandardSecurityGroupItemType> SecurityGroup;

	public List<StandardSecurityGroupItemType> getSecurityGroup() {
		return SecurityGroup;
	}

	public void setSecurityGroup(List<StandardSecurityGroupItemType> securityGroup) {
		SecurityGroup = securityGroup;
	}
}