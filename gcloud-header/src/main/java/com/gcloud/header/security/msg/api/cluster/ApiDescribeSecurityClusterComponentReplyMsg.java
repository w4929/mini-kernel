package com.gcloud.header.security.msg.api.cluster;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.security.model.DescribeSecurityClusterComponentResponse;
import com.gcloud.header.security.model.SecurityClusterComponentType;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiDescribeSecurityClusterComponentReplyMsg extends PageReplyMessage<SecurityClusterComponentType>{

	private static final long serialVersionUID = 1L;

	@ApiModel(description = "安全集群组件列表")
	private DescribeSecurityClusterComponentResponse securityClusterComponents;
	
	@Override
	public void setList(List<SecurityClusterComponentType> list) {
		securityClusterComponents = new DescribeSecurityClusterComponentResponse();
		securityClusterComponents.setSecurityClusterComponent(list);
	}

	public DescribeSecurityClusterComponentResponse getSecurityClusterComponents() {
		return securityClusterComponents;
	}

	public void setSecurityClusterComponents(DescribeSecurityClusterComponentResponse securityClusterComponents) {
		this.securityClusterComponents = securityClusterComponents;
	}
}