package com.gcloud.header.slb.msg.api;

import java.io.Serializable;

import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ListenerPortAndProtocol implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "监听器ID")
	private String listenerId;
	@ApiModel(description = "端口�?")
	private Integer listenerPort;
	@ApiModel(description = "协议")
	private String listenerProtocol;
	@ApiModel(description = "监听器绑定的后端服务器ID")
	private String vServerGroupId;
	@ApiModel(description = "监听器绑定的后端服务器名�?")
	private String vServerGroupName;
	
	public String getListenerId() {
		return listenerId;
	}
	public void setListenerId(String listenerId) {
		this.listenerId = listenerId;
	}
	public Integer getListenerPort() {
		return listenerPort;
	}
	public void setListenerPort(Integer listenerPort) {
		this.listenerPort = listenerPort;
	}
	public String getListenerProtocol() {
		return listenerProtocol;
	}
	public void setListenerProtocol(String listenerProtocol) {
		this.listenerProtocol = listenerProtocol;
	}
	public String getvServerGroupId() {
		return vServerGroupId;
	}
	public void setvServerGroupId(String vServerGroupId) {
		this.vServerGroupId = vServerGroupId;
	}
	public String getvServerGroupName() {
		return vServerGroupName;
	}
	public void setvServerGroupName(String vServerGroupName) {
		this.vServerGroupName = vServerGroupName;
	}
}