package com.gcloud.header.slb.msg.api.standard;

import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class StandardApiRemoveVServerGroupBackendServersMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "服务器组ID", require = true)
	@NotBlank(message = "0140701::后端服务器组ID不能为空")
	private String vServerGroupId;
	@ApiModel(description = "要删除的后端服务器列�?", require = true)
	@NotBlank(message = "0140702::后端服务器信息不能为�?")
	private String backendServers;

	@Override
	public Class replyClazz() {
		return StandardApiRemoveVServerGroupBackendServersReplyMsg.class;
	}

	public String getvServerGroupId() {
		return vServerGroupId;
	}

	public void setvServerGroupId(String vServerGroupId) {
		this.vServerGroupId = vServerGroupId;
	}

	public String getBackendServers() {
		return backendServers;
	}

	public void setBackendServers(String backendServers) {
		this.backendServers = backendServers;
	}
}