package com.gcloud.header.slb.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gcloud.framework.db.jdbc.annotation.TableField;
import com.gcloud.header.GcloudConstants;
import com.gcloud.header.api.ApiIgnore;
import com.gcloud.header.api.ApiModel;

import java.io.Serializable;
import java.util.Date;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class LoadBalancerModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModel(description = "负载均衡ID")
    @TableField("id")
    private String loadBalancerId;
    @ApiModel(description = "负载均衡实例别名")
    @TableField("name")
    private String loadBalancerName;
    @ApiModel(description = "负载均衡实例状�??")
    @TableField("status")
    private String loadBalancerStatus;
    @ApiModel(description = "负载均衡实例中文状�??")
    private String loadBalancerCnStatus;
    @ApiModel(description = "负载均衡实例服务地址")
    private String address;
    @ApiModel(description = "负载均衡实例Vpcid")
    private String vpcId;
    @ApiModel(description = "网络�?")
    private String vpcName;
    @ApiModel(description = "负载均衡实例Vswitchid")
    @TableField("vip_subnet_id")
    private String vSwitchId;
    @ApiModel(description = "子网名称")
    private String vSwitchName;
    @ApiModel(description = "创建时间")
    @JsonFormat(timezone = GcloudConstants.DEFAULT_TIMEZONE, pattern = GcloudConstants.DEFAULT_DATEFORMAT)
    private Date createTime;
    @ApiModel(description = "负载均衡监听器ID")
    private String listener;
    @ApiModel(description = "创建�?")
    @TableField("user_id")
    private String creator;
    
    public String getLoadBalancerCnStatus() {
		return loadBalancerCnStatus;
	}

	public void setLoadBalancerCnStatus(String loadBalancerCnStatus) {
		this.loadBalancerCnStatus = loadBalancerCnStatus;
	}

	public String getLoadBalancerId() {
        return loadBalancerId;
    }

    public void setLoadBalancerId(String loadBalancerId) {
        this.loadBalancerId = loadBalancerId;
    }

    public String getLoadBalancerName() {
        return loadBalancerName;
    }

    public void setLoadBalancerName(String loadBalancerName) {
        this.loadBalancerName = loadBalancerName;
    }

    public String getLoadBalancerStatus() {
        return loadBalancerStatus;
    }

    public void setLoadBalancerStatus(String loadBalancerStatus) {
        this.loadBalancerStatus = loadBalancerStatus;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getVpcId() {
        return vpcId;
    }

    public void setVpcId(String vpcId) {
        this.vpcId = vpcId;
    }

    public String getVpcName() {
		return vpcName;
	}

	public void setVpcName(String vpcName) {
		this.vpcName = vpcName;
	}

	public String getvSwitchId() {
        return vSwitchId;
    }

    public void setvSwitchId(String vSwitchId) {
        this.vSwitchId = vSwitchId;
    }

    public String getvSwitchName() {
		return vSwitchName;
	}

	public void setvSwitchName(String vSwitchName) {
		this.vSwitchName = vSwitchName;
	}

	public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getListener() {
        return listener;
    }

    public void setListener(String listener) {
        this.listener = listener;
    }

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}
    
}