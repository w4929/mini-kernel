package com.gcloud.header.slb.msg.api.standard;

import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class StandardApiSetHealthCheckAttributeMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "监听器ID或后端服务器组ID", require = true)
	@NotBlank(message = "0150201::监听器ID或后端服务器组ID不能为空")
	private String resourceId;
	@ApiModel(description = "监听器协议，取�?�：TCP| HTTP | HTTPS", require = false)
	private String protocol;
	@ApiModel(description = "健康�?查连续失败次�?", require = false)
	private Integer unhealthyThreshold;
	@ApiModel(description = "健康�?查等待后端服务器响应时间", require = false)
	private Integer healthCheckTimeout;
	@ApiModel(description = "健康�?查的时间间隔", require = false)
	private Integer healthCheckInterval;
	@NotBlank(message = "0150202::健康�?查开关不能为�?")
	@ApiModel(description = "健康�?查开关：on、off", require = true)
	private String healthCheck;
	@ApiModel(description = "用于健康�?查的URI", require = false)
	private String healthCheckURI;
	@ApiModel(description = "健康�?查连续成功次�?", require = false)
	private Integer healthyThreshold;
	@ApiModel(description = "健康�?查类�?,取�?�：tcp | http", require = false)
	private String healthCheckType;

	@Override
	public Class replyClazz() {
		return StandardApiSetHealthCheckAttributeReplyMsg.class;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public Integer getUnhealthyThreshold() {
		return unhealthyThreshold;
	}

	public void setUnhealthyThreshold(Integer unhealthyThreshold) {
		this.unhealthyThreshold = unhealthyThreshold;
	}

	public Integer getHealthCheckTimeout() {
		return healthCheckTimeout;
	}

	public void setHealthCheckTimeout(Integer healthCheckTimeout) {
		this.healthCheckTimeout = healthCheckTimeout;
	}

	public Integer getHealthCheckInterval() {
		return healthCheckInterval;
	}

	public void setHealthCheckInterval(Integer healthCheckInterval) {
		this.healthCheckInterval = healthCheckInterval;
	}

	public String getHealthCheck() {
		return healthCheck;
	}

	public void setHealthCheck(String healthCheck) {
		this.healthCheck = healthCheck;
	}

	public String getHealthCheckURI() {
		return healthCheckURI;
	}

	public void setHealthCheckURI(String healthCheckURI) {
		this.healthCheckURI = healthCheckURI;
	}

	public Integer getHealthyThreshold() {
		return healthyThreshold;
	}

	public void setHealthyThreshold(Integer healthyThreshold) {
		this.healthyThreshold = healthyThreshold;
	}

	public String getHealthCheckType() {
		return healthCheckType;
	}

	public void setHealthCheckType(String healthCheckType) {
		this.healthCheckType = healthCheckType;
	}
}