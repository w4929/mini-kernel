package com.gcloud.controller.image.workflow;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.controller.image.dao.ImageDao;
import com.gcloud.controller.image.dao.ImagePropertyDao;
import com.gcloud.controller.image.dao.ImageStoreDao;
import com.gcloud.controller.image.driver.ImageDriverEnum;
import com.gcloud.controller.image.entity.Image;
import com.gcloud.controller.image.entity.ImageStore;
import com.gcloud.controller.image.prop.ImageProp;
import com.gcloud.controller.image.service.IImageService;
import com.gcloud.controller.image.workflow.model.DeleteGcloudImageFlowCommandReq;
import com.gcloud.controller.image.workflow.model.DeleteGcloudImageFlowCommandRes;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Component
@Scope("prototype")
@Slf4j
public class DeleteGcloudImageFlowCommand extends BaseWorkFlowCommand {
	
	@Autowired
    ImageProp prop;
	
	@Autowired
    private IImageService imageService;

//    @Autowired
//    private ImagePropertyDao imagePropertyDao;
    
    @Autowired
	private ImageStoreDao storeDao;

	@Override
	protected Object process() throws Exception {
		DeleteGcloudImageFlowCommandReq req = (DeleteGcloudImageFlowCommandReq)getReqParams();
		/*ImageDriverEnum.getByType(prop.getStroageType()).deleteImage(req.getImageId());
		
		imageDao.deleteById(req.getImageId());
        imagePropertyDao.deleteByImageId(req.getImageId());*/
		imageService.deleteImage(req.getImageId());
        
        List<ImageStore> stores = storeDao.findByProperty("imageId", req.getImageId());
        DeleteGcloudImageFlowCommandRes res = new DeleteGcloudImageFlowCommandRes();
        res.setTaskId(getTaskId());
        res.setStores(stores);
		return res;
	}

	@Override
	protected Object rollback() throws Exception {
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		DeleteGcloudImageFlowCommandReq req = (DeleteGcloudImageFlowCommandReq)getReqParams();
		Image img = imageService.getById(req.getImageId());
		if(img == null) {
			return true;
		}
		return false;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return DeleteGcloudImageFlowCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return DeleteGcloudImageFlowCommandRes.class;
	}

}