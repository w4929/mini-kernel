package com.gcloud.controller.image.driver;


import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */



public interface IImageStoreDriver {
	void copyImage(String sourceFilePath, String imageId, String resourceType);//暂时不�?�虑image存储后端为lvm的情�?
	void deleteImage(String imageId, String resourceType);
	/**分发镜像
	 * @param imageId
	 * @param target  可为节点名�?�vg�?
	 * @param targetType node、vg
	 */
	void distributeImage(String imageId, String target, String targetType, String taskId, String resourceType, String distributeAction);
	
	/** 获取镜像文件实际大小，单�? byte
	 * @param imageId
	 * @return
	 */
	long getImageActualSize(String imageId);

	Resource download(String imageId);

	void upload(String imageId, MultipartFile file);
	
//	void deleteImageCache(String imageId);
	
}