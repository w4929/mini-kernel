package com.gcloud.controller.image.controller;

import com.gcloud.common.constants.HttpRequestConstant;
import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.image.model.UploadImageParams;
import com.gcloud.controller.image.model.UploadImageResponse;
import com.gcloud.controller.image.service.IImageService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.identity.IApiIdentity;
import com.gcloud.core.identity.TokenUser;
import com.gcloud.header.api.model.CurrentUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@RestController
@RequestMapping("/image")
@Slf4j
public class ImageController {

    @Autowired
    private IImageService imageService;

    @Autowired
    private IApiIdentity apiIdentity;

    @RequestMapping("/download.do")
    public ResponseEntity<Resource> download(@RequestParam("imageId") String imageId, HttpServletRequest request) {

        //暂时直接做校验，后续等认证完善再做处�?
        String tokenId = request.getHeader(HttpRequestConstant.HEADER_TOKEN_ID);
        checkToken(tokenId);

        // Load file as Resource
        Resource resource = imageService.download(imageId);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            log.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    @PostMapping("/upload.do")
    public ResponseEntity<UploadImageResponse> upload(UploadImageParams params, HttpServletRequest request){

        //暂时直接做校验，后续等认证完善再做处�?
        String tokenId = request.getHeader(HttpRequestConstant.HEADER_TOKEN_ID);
        TokenUser tokenUser = checkToken(tokenId);
        //后面只用到userId
        CurrentUser currentUser = new CurrentUser();
        currentUser.setId(tokenUser.getUserInfo().getId());
        params.setCurrentUser(currentUser);

        String imageId = imageService.upload(params);
        UploadImageResponse response = new UploadImageResponse();
        response.setImageId(imageId);

        return ResponseEntity.ok(response);
    }

    public TokenUser checkToken(String token){
        TokenUser tokenUser = null;
        try{
            tokenUser = apiIdentity.checkToken(token);
        }catch (Exception ex){
            log.error("获取用户信息失败, ex=" + ex, ex);
            throw new GCloudException("::认证信息无效");
        }


        if(tokenUser == null || StringUtils.isBlank(tokenUser.getUserInfo().getId())){
            throw new GCloudException("::认证信息无效");
        }

        return tokenUser;
    }

}