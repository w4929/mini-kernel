package com.gcloud.controller.image.workflow.model;

import java.util.List;

import com.gcloud.controller.image.entity.ImageStore;
import com.gcloud.core.workflow.model.WorkflowFirstStepResException;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DeleteGcloudImageFlowCommandRes extends WorkflowFirstStepResException{
	List<ImageStore> stores;
	private String taskId;

	public List<ImageStore> getStores() {
		return stores;
	}

	public void setStores(List<ImageStore> stores) {
		this.stores = stores;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	
}