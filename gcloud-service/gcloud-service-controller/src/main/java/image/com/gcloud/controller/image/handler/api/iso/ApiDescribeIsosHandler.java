package com.gcloud.controller.image.handler.api.iso;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.image.model.iso.DescribeIsoParams;
import com.gcloud.controller.image.service.IIsoService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.image.model.iso.IsoType;
import com.gcloud.header.image.msg.api.iso.ApiDescribeIsosMsg;
import com.gcloud.header.image.msg.api.iso.ApiDescribeIsosReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module= Module.ECS, subModule=SubModule.IMAGE, action="DescribeIsos",name="映像列表")
public class ApiDescribeIsosHandler extends MessageHandler<ApiDescribeIsosMsg, ApiDescribeIsosReplyMsg>{
	@Autowired
    private IIsoService isoService;
	
	@Override
	public ApiDescribeIsosReplyMsg handle(ApiDescribeIsosMsg msg) throws GCloudException {
		DescribeIsoParams params = BeanUtil.copyProperties(msg, DescribeIsoParams.class);
        if(msg.getDisable() == null) {
        	params.setDisable(null);
        }
        PageResult<IsoType> response = isoService.describeIso(params, msg.getCurrentUser());
        ApiDescribeIsosReplyMsg reply = new ApiDescribeIsosReplyMsg();
        reply.init(response);

        return reply;
	}

}