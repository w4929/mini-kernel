package com.gcloud.controller.image.distribute;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gcloud.controller.image.driver.ImageDriverEnum;
import com.gcloud.controller.image.enums.ImageDistributeTargetType;
import com.gcloud.controller.image.prop.ImageProp;
import com.gcloud.controller.image.service.IImageStoreService;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.service.ServiceName;
import com.gcloud.header.enums.ProviderType;
import com.gcloud.header.image.enums.ImageResourceType;
import com.gcloud.header.image.msg.node.DownloadImageMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
public class FileToRbdDistributeImageImpl implements IDistributeImage {
	
	@Autowired
	ImageProp prop;
	
	@Autowired
	ServiceName serviceName;
	
	@Autowired
	IImageStoreService imageStoreService;
	
	@Autowired
	private MessageBus bus;

	@Override
	public void distributeImage(String imageId, String target, String taskId, String resourceType, String distributeAction) {
		//String controllerNode = MessageUtil.controllerServiceId().substring(MessageUtil.controllerServiceId().indexOf("-") + 1);
		DownloadImageMsg downloadMsg = new DownloadImageMsg();
        downloadMsg.setServiceId(serviceName.getImageSchedule());
        downloadMsg.setImageId(imageId);
        downloadMsg.setImagePath((resourceType.equals(ImageResourceType.IMAGE.value())?prop.getImageFilesystemStoreDir():prop.getIsoFilesystemStoreDir()) + imageId);
        downloadMsg.setProvider(ProviderType.GCLOUD.name().toLowerCase());
        downloadMsg.setImageStroageType(ImageDriverEnum.FILE.getStorageType());
        downloadMsg.setTarget(target);
        downloadMsg.setTargetType(ImageDistributeTargetType.RBD.value());
        downloadMsg.setTaskId(taskId);
        downloadMsg.setImageResourceType(resourceType);
        downloadMsg.setDistributeAction(distributeAction);
        
        bus.send(downloadMsg);
	}
	
	/*private void exec(String[] cmd, String errorCode) {
		int res = SystemUtil.runAndGetCode(cmd);
		if(res != 0) {
			throw new GCloudException(errorCode);
		}
	}*/

	/*@Override
	public void deleteImageCache(String imageId, String target,String storeType, String taskId) {
		//删掉images池上对应的image cache , rbd rm  -p images  imageId 
		String[] deleteCmd = new String[]{"rbd", "rm", "-p", "images" , imageId};
        int deleteRes = SystemUtil.runAndGetCode(deleteCmd);
        if(deleteRes != 0) {
        	throw new GCloudException("::删除images池上的镜�?" + imageId + "失败");
        }
	}*/

}