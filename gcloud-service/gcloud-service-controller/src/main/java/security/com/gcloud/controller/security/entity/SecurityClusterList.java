package com.gcloud.controller.security.entity;

import java.util.List;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Table(name = "gc_security_cluster")
public class SecurityClusterList {
	@ID
	private String id;
	private String name;
	private String protectionNetCidr;
	private String state;
	private String stateCnName;
	private String createUser;
	private String createUserName;
	private String createTime;
	private List<SecurityClusterComponent> securityClusterComponents;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getProtectionNetCidr() {
		return protectionNetCidr;
	}
	public void setProtectionNetCidr(String protectionNetCidr) {
		this.protectionNetCidr = protectionNetCidr;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getStateCnName() {
		return stateCnName;
	}
	public void setStateCnName(String stateCnName) {
		this.stateCnName = stateCnName;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public String getCreateUserName() {
		return createUserName;
	}
	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public List<SecurityClusterComponent> getSecurityClusterComponents() {
		return securityClusterComponents;
	}
	public void setSecurityClusterComponents(List<SecurityClusterComponent> securityClusterComponents) {
		this.securityClusterComponents = securityClusterComponents;
	}
}