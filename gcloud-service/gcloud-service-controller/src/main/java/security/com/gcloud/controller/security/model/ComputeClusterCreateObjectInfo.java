package com.gcloud.controller.security.model;


import com.gcloud.controller.security.enums.SecurityClusterComponentObjectType;
import com.gcloud.controller.security.enums.SecurityComponent;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class ComputeClusterCreateObjectInfo {

    private String componentId;
    private SecurityComponent component;
    private ClusterCreateVmInfo createVm;
    private ClusterCreateDcInfo createDc;
    private SecurityClusterComponentObjectType objectType;

    public String getComponentId() {
        return componentId;
    }

    public void setComponentId(String componentId) {
        this.componentId = componentId;
    }

    public SecurityComponent getComponent() {
        return component;
    }

    public void setComponent(SecurityComponent component) {
        this.component = component;
    }

    public ClusterCreateVmInfo getCreateVm() {
        return createVm;
    }

    public void setCreateVm(ClusterCreateVmInfo createVm) {
        this.createVm = createVm;
    }

    public ClusterCreateDcInfo getCreateDc() {
        return createDc;
    }

    public void setCreateDc(ClusterCreateDcInfo createDc) {
        this.createDc = createDc;
    }

    public SecurityClusterComponentObjectType getObjectType() {
        return objectType;
    }

    public void setObjectType(SecurityClusterComponentObjectType objectType) {
        this.objectType = objectType;
    }
}