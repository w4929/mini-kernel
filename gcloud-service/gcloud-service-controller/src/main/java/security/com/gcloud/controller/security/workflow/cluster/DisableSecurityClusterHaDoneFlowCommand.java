package com.gcloud.controller.security.workflow.cluster;

import com.gcloud.controller.security.dao.SecurityClusterDao;
import com.gcloud.controller.security.entity.SecurityCluster;
import com.gcloud.controller.security.enums.SecurityClusterState;
import com.gcloud.controller.security.model.workflow.DisableSecurityClusterHaDoneFlowCommandReq;
import com.gcloud.controller.security.model.workflow.DisableSecurityClusterHaDoneFlowCommandRes;
import com.gcloud.controller.security.service.ISecurityClusterService;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@Scope("prototype")
@Slf4j
public class DisableSecurityClusterHaDoneFlowCommand extends BaseWorkFlowCommand {

    @Autowired
    private SecurityClusterDao securityClusterDao;

    @Autowired
    private ISecurityClusterService securityClusterService;


    @Override
    protected Object process() throws Exception {

        DisableSecurityClusterHaDoneFlowCommandReq req = (DisableSecurityClusterHaDoneFlowCommandReq)getReqParams();

        List<String> updateField = new ArrayList<>();
        SecurityCluster cluster = new SecurityCluster();
        cluster.setId(req.getClusterId());
        updateField.add(cluster.updateState(SecurityClusterState.CREATED.value()));
        updateField.add(cluster.updateUpdateTime(new Date()));
        updateField.add(cluster.updateHa(false));

        securityClusterDao.update(cluster, updateField);

        securityClusterService.cleanClusterHaData(req.getClusterId());

        return null;
    }

    @Override
    protected Object rollback() throws Exception {
        return null;
    }

    @Override
    protected Object timeout() throws Exception {
        return null;
    }

    @Override
    protected Class<?> getReqParamClass() {
        return DisableSecurityClusterHaDoneFlowCommandReq.class;
    }

    @Override
    protected Class<?> getResParamClass() {
        return DisableSecurityClusterHaDoneFlowCommandRes.class;
    }
}