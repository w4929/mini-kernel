package com.gcloud.controller.security.handler.api.cluster;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.security.model.SecurityClusterTopologyParams;
import com.gcloud.controller.security.service.ISecurityClusterService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.security.model.SecurityClusterTopologyResponse;
import com.gcloud.header.security.msg.api.cluster.ApiSecurityClusterTopologyMsg;
import com.gcloud.header.security.msg.api.cluster.ApiSecurityClusterTopologyReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module = Module.ECS, subModule= SubModule.SECURITYCLUSTER, action = "SecurityClusterTopology", name = "安全集群拓扑")
public class ApiSecurityClusterTopologyHandler extends MessageHandler<ApiSecurityClusterTopologyMsg, ApiSecurityClusterTopologyReplyMsg>	{

	@Autowired
	private ISecurityClusterService securityClusterService;
	
	@Override
	public ApiSecurityClusterTopologyReplyMsg handle(ApiSecurityClusterTopologyMsg msg) throws GCloudException {
		SecurityClusterTopologyParams params = BeanUtil.copyProperties(msg, SecurityClusterTopologyParams.class);
		SecurityClusterTopologyResponse response = securityClusterService.securityClusterTopology(params, msg.getCurrentUser());
    	
    	ApiSecurityClusterTopologyReplyMsg reply = new ApiSecurityClusterTopologyReplyMsg();
    	reply.setSecurityClusterTopology(response);
        return reply;
	}

}