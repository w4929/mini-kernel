package com.gcloud.controller.security.model;

import com.gcloud.controller.security.model.workflow.CreateClusterComponentObjectInfo;
import com.gcloud.controller.security.model.workflow.SecurityClusterComponentHaNetcardInfo;

import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class EnableClusterHaResponse {

    private String id;
    private List<CreateClusterComponentObjectInfo> components;
    private List<ClusterCreateOvsBridgeInfo> bridgeInfos;
    private List<SecurityClusterComponentHaNetcardInfo> haNetcardInfos;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<CreateClusterComponentObjectInfo> getComponents() {
        return components;
    }

    public void setComponents(List<CreateClusterComponentObjectInfo> components) {
        this.components = components;
    }

    public List<ClusterCreateOvsBridgeInfo> getBridgeInfos() {
        return bridgeInfos;
    }

    public void setBridgeInfos(List<ClusterCreateOvsBridgeInfo> bridgeInfos) {
        this.bridgeInfos = bridgeInfos;
    }

    public List<SecurityClusterComponentHaNetcardInfo> getHaNetcardInfos() {
        return haNetcardInfos;
    }

    public void setHaNetcardInfos(List<SecurityClusterComponentHaNetcardInfo> haNetcardInfos) {
        this.haNetcardInfos = haNetcardInfos;
    }
}