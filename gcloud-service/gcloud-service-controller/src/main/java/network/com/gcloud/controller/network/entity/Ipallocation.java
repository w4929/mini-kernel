package com.gcloud.controller.network.entity;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Table(name = "gc_ipallocations", jdbc = "controllerJdbcTemplate")
public class Ipallocation {

    @ID
    private Long id;
    private String portId;
    private String ipAddress;
    private String subnetId;
    private String networkId;

    public static final String ID = "id";
    public static final String PORT_ID = "portId";
    public static final String IP_ADDRESS = "ipAddress";
    public static final String SUBNET_ID = "subnetId";
    public static final String NETWORK_ID = "networkId";

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPortId() {
        return portId;
    }

    public void setPortId(String portId) {
        this.portId = portId;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getSubnetId() {
        return subnetId;
    }

    public void setSubnetId(String subnetId) {
        this.subnetId = subnetId;
    }

    public String getNetworkId() {
        return networkId;
    }

    public void setNetworkId(String networkId) {
        this.networkId = networkId;
    }

    public String updateId(Long id) {
        this.setId(id);
        return ID;
    }

    public String updatePortId(String portId) {
        this.setPortId(portId);
        return PORT_ID;
    }

    public String updateIpAddress(String ipAddress) {
        this.setIpAddress(ipAddress);
        return IP_ADDRESS;
    }

    public String updateSubnetId(String subnetId) {
        this.setSubnetId(subnetId);
        return SUBNET_ID;
    }

    public String updateNetworkId(String networkId) {
        this.setNetworkId(networkId);
        return NETWORK_ID;
    }
}