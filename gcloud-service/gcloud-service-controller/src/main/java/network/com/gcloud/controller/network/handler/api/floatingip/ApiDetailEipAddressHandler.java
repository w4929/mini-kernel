package com.gcloud.controller.network.handler.api.floatingip;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.controller.network.service.IFloatingIpService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.api.ApiTypeAnno;
import com.gcloud.header.common.ApiTypeConst;
import com.gcloud.header.network.msg.api.ApiDetailEipAddressMsg;
import com.gcloud.header.network.msg.api.ApiDetailEipAddressReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiTypeAnno(apiType=ApiTypeConst.QUERY)
@ApiHandler(module=Module.ECS,subModule=SubModule.EIPADDRSS,action="DetailEipAddress",name="弹�?�公网IP地址详情")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.EIP, resourceIdField = "allocationId")
public class ApiDetailEipAddressHandler extends MessageHandler<ApiDetailEipAddressMsg, ApiDetailEipAddressReplyMsg>{
	@Autowired
	IFloatingIpService eipService;

	@Override
	public ApiDetailEipAddressReplyMsg handle(ApiDetailEipAddressMsg msg) throws GCloudException {
		ApiDetailEipAddressReplyMsg reply = new ApiDetailEipAddressReplyMsg();
		reply.setDetailEipAddress(eipService.detail(msg));
		return reply;
	}
}