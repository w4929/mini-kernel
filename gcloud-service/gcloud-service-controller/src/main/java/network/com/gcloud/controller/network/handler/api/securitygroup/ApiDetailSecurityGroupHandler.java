package com.gcloud.controller.network.handler.api.securitygroup;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.network.service.ISecurityGroupService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.api.ApiTypeAnno;
import com.gcloud.header.common.ApiTypeConst;
import com.gcloud.header.network.model.DetailSecurityGroupResponse;
import com.gcloud.header.network.msg.api.ApiDetailSecurityGroupMsg;
import com.gcloud.header.network.msg.api.ApiDetailSecurityGroupReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiTypeAnno(apiType=ApiTypeConst.QUERY)
@ApiHandler(module=Module.ECS,subModule=SubModule.SECURITYGROUP,action="DetailSecurityGroup",name="安全组详�?")
public class ApiDetailSecurityGroupHandler extends MessageHandler<ApiDetailSecurityGroupMsg, ApiDetailSecurityGroupReplyMsg>{
	@Autowired
	ISecurityGroupService securityGroupService;
	
	@Override
	public ApiDetailSecurityGroupReplyMsg handle(ApiDetailSecurityGroupMsg msg) throws GCloudException {
		DetailSecurityGroupResponse response = securityGroupService.detail(msg.getSecurityGroupId());
		ApiDetailSecurityGroupReplyMsg reply = new ApiDetailSecurityGroupReplyMsg();
		reply.setDetailSecurityGroup(response);
		return reply;
	}

}