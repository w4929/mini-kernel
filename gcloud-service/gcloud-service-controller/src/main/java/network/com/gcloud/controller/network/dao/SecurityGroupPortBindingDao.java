package com.gcloud.controller.network.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gcloud.controller.network.entity.SecurityGroupPortBinding;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Repository
public class SecurityGroupPortBindingDao extends JdbcBaseDaoImpl<SecurityGroupPortBinding, Long> {
	public void deleteByPortSecurityGroup(String portId, String securityGroupId) {
		String sql = "delete from gc_security_group_port_bindings ";
		sql += "where port_id='" + portId + "' ";
		if(null != securityGroupId) {
			sql += "and security_group_id='" + securityGroupId + "'";
		}
		
	    this.jdbcTemplate.execute(sql);
	}
	
	public void deleteBySecurityGroup(String securityGroupId) {
		String sql = "delete from gc_security_group_port_bindings ";
		sql += "where security_group_id='" + securityGroupId + "' ";
		
	    this.jdbcTemplate.execute(sql);
	}
	
    public int deleteByPortId(String portId) {
        String sql = "delete from gc_security_group_port_bindings where port_id = ?";
        Object[] values = {portId};
        return this.jdbcTemplate.update(sql, values);
    }
    
    public <E>List<E> findInfosByPortId(String portId, Class<E> clazz) {
    	StringBuffer sql = new StringBuffer();
    	List<Object> values = new ArrayList<>();
    	sql.append("select sgp.*, sg.id as securityGroupId, sg.name as securityGroupName from gc_security_group_port_bindings as sgp ");
    	sql.append(" left join gc_security_groups as sg on sgp.security_group_id = sg.id ");
    	sql.append(" where sgp.port_id = ?");
    	values.add(portId);
    	
    	return findBySql(sql.toString(), values, clazz);
    }

}