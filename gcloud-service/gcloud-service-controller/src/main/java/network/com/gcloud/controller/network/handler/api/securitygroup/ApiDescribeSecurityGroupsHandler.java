package com.gcloud.controller.network.handler.api.securitygroup;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.common.model.PageParams;
import com.gcloud.controller.network.model.DescribeSecurityGroupsParams;
import com.gcloud.controller.network.service.ISecurityGroupService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.model.SecurityGroupItemType;
import com.gcloud.header.network.msg.api.DescribeSecurityGroupsMsg;
import com.gcloud.header.network.msg.api.DescribeSecurityGroupsReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module=Module.ECS,subModule=SubModule.SECURITYGROUP,action="DescribeSecurityGroups", name = "安全组列�?")
public class ApiDescribeSecurityGroupsHandler extends MessageHandler<DescribeSecurityGroupsMsg, DescribeSecurityGroupsReplyMsg> {
	@Autowired
	ISecurityGroupService securityGroupService;
	
	@Override
	public DescribeSecurityGroupsReplyMsg handle(DescribeSecurityGroupsMsg msg) throws GCloudException {
		DescribeSecurityGroupsParams params = BeanUtil.copyProperties(msg, DescribeSecurityGroupsParams.class);
        PageResult<SecurityGroupItemType> response = securityGroupService.describeSecurityGroups(params, msg.getCurrentUser());
        DescribeSecurityGroupsReplyMsg replyMsg = new DescribeSecurityGroupsReplyMsg();
        replyMsg.init(response);
        return replyMsg;
	}

}