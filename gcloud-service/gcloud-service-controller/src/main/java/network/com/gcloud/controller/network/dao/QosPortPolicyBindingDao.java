package com.gcloud.controller.network.dao;

import com.gcloud.controller.network.entity.QosPortPolicyBinding;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import org.springframework.stereotype.Repository;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Repository
public class QosPortPolicyBindingDao extends JdbcBaseDaoImpl<QosPortPolicyBinding, Long> {

    public int deleteByPortId(String portId){

        String sql = "delete from gc_qos_port_policy_bindings where port_id = ?";
        Object[] values = {portId};
        return this.jdbcTemplate.update(sql, values);
    }

    public int deleteByPolicyId(String policyId){

        String sql = "delete from gc_qos_port_policy_bindings where policy_id = ?";
        Object[] values = {policyId};
        return this.jdbcTemplate.update(sql, values);
    }

}