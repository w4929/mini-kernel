package com.gcloud.controller.network.service.impl;

import com.gcloud.controller.ResourceProviders;
import com.gcloud.controller.network.dao.PortDao;
import com.gcloud.controller.network.dao.QosPolicyDao;
import com.gcloud.controller.network.dao.QosPortPolicyBindingDao;
import com.gcloud.controller.network.entity.Port;
import com.gcloud.controller.network.entity.QosPolicy;
import com.gcloud.controller.network.entity.QosPortPolicyBinding;
import com.gcloud.controller.network.provider.IQosProvider;
import com.gcloud.controller.network.service.IQosPortPolicyBindingService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.header.enums.ResourceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class QosPortPolicyBindingServiceImpl implements IQosPortPolicyBindingService {

    @Autowired
    private PortDao portDao;

    @Autowired
    private QosPolicyDao qosPolicyDao;

    @Autowired
    private QosPortPolicyBindingDao qosPortPolicyBindingDao;

    @Override
    public void bind(String portId, String policyId) {

        QosPolicy qosPolicy = qosPolicyDao.getById(policyId);
        if(qosPolicy == null){
            throw new GCloudException("::qos 不存�?");
        }
        Port port = portDao.getById(portId);
        if(port == null){
            throw new GCloudException("::port 不存�?");
        }

        QosPortPolicyBinding bind = new QosPortPolicyBinding();
        bind.setPolicyId(policyId);
        bind.setPortId(portId);
        qosPortPolicyBindingDao.save(bind);

        IQosProvider qosProvider = checkAndGetProvider(port.getProvider());
        qosProvider.bindPort(port.getProviderRefId(), qosPolicy.getProviderRefId());
    }

    private IQosProvider checkAndGetProvider(Integer providerType) {
        IQosProvider provider = ResourceProviders.checkAndGet(ResourceType.QOS, providerType);
        return provider;
    }
}