package com.gcloud.controller.network.service.impl;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.ResourceProviders;
import com.gcloud.controller.network.dao.NetworkDao;
import com.gcloud.controller.network.dao.PortDao;
import com.gcloud.controller.network.dao.SubnetDao;
import com.gcloud.controller.network.entity.Network;
import com.gcloud.controller.network.entity.Port;
import com.gcloud.controller.network.enums.NetworkType;
import com.gcloud.controller.network.model.CreateNetworkParams;
import com.gcloud.controller.network.model.DetailExternalNetworkParams;
import com.gcloud.controller.network.provider.INetworkProvider;
import com.gcloud.controller.network.service.INetworkService;
import com.gcloud.controller.network.service.IPortService;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.compute.enums.DeviceOwner;
import com.gcloud.header.enums.ResourceType;
import com.gcloud.header.network.enums.NetworkStatus;
import com.gcloud.header.network.model.DetailExternalNetwork;
import com.gcloud.header.network.model.ExternalNetworkSetType;
import com.gcloud.header.network.model.TplExternalNetworkItem;
import com.gcloud.header.network.model.TplExternalNetworkResponse;
import com.gcloud.header.network.model.TplVSwitchItem;
import com.gcloud.header.network.model.TplVSwitchItems;
import com.gcloud.header.network.model.VSwitchIds;
import com.gcloud.header.network.model.VpcsItemType;
import com.gcloud.header.network.msg.api.ApiTplExternalNetworksMsg;
import com.gcloud.header.network.msg.api.CreateExternalNetworkMsg;
import com.gcloud.header.network.msg.api.DescribeExternalNetworksMsg;
import com.gcloud.header.network.msg.api.DescribeVpcsMsg;
import com.gcloud.header.network.msg.api.ModifyVpcAttributeMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Service
@Transactional(propagation = Propagation.REQUIRED)
public class NetworkServiceImpl implements INetworkService {
    @Autowired
    private NetworkDao networkDao;

    @Autowired
    private IPortService portService;

    @Autowired
    private SubnetDao subnetDao;

    @Autowired
    private PortDao portDao;

    public PageResult<VpcsItemType> describeVpcs(DescribeVpcsMsg msg) {
        return networkDao.describeVpcs(msg);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public String createNetwork(CreateNetworkParams params, CurrentUser currentUser) {
        String networkId = StringUtils.genUuid();
        this.getProviderOrDefault().createNetwork(networkId, params, currentUser);
        return networkId;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public String createExternalNetwork(CreateExternalNetworkMsg msg) {
        String networkId = StringUtils.genUuid();
        this.getProviderOrDefault().createExternalNetwork(networkId, msg);
        return networkId;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void removeNetwork(String networkId) {
        Network network = networkDao.getById(networkId);
        if(network == null) {
        	throw new GCloudException("0160202::ç½ç»ä¸å­å?");
        }

        if(subnetDao.hasSubnet(networkId)){
            throw new GCloudException("0160203::æå³èçå­ç½ï¼ä¸è½å é?");
        }

        List<Port> ports = portDao.findByProperty(Port.NETWORK_ID, networkId);
        if(ports != null && ports.size() > 0){
            for(Port port : ports){

                if(DeviceOwner.GATEWAY.getValue().equals(port.getDeviceOwner())){
                    throw new GCloudException("0160204::æå³èçè·¯ç±ç½å³ï¼ä¸è½å é?");
                //ä»¥ådhcpï¼æ¯åå»ºçæ¶ååæ»è¿æ²¡å é?
                }else if(!DeviceOwner.DHCP.getValue().equals(port.getDeviceOwner())){
                    throw new GCloudException("0160205::æå³èçç«¯å£ï¼ä¸è½å é?");
                }
            }
        }

    	networkDao.deleteById(networkId);
        this.checkAndGetProvider(network.getProvider()).removeNetwork(network.getProviderRefId());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateNetwork(ModifyVpcAttributeMsg msg) {
        Network network = networkDao.getById(msg.getVpcId());
        if (network == null) {
            throw new GCloudException("0160303::ç½ç»ä¸å­å?");
        }
        List<String> updateField = new ArrayList<String>();
        updateField.add(network.updateName(msg.getVpcName()));
        updateField.add(network.updateUpdatedAt(new Date()));
        networkDao.update(network, updateField);
        CacheContainer.getInstance().put(CacheType.NETWORK_NAME, msg.getVpcId(), msg.getVpcName());
       
        this.checkAndGetProvider(network.getProvider()).updateNetwork(network.getProviderRefId(), msg.getVpcName());
    
    }
    
    public PageResult<ExternalNetworkSetType> describeNetworks(DescribeExternalNetworksMsg msg) {
    	//æ¥è¯¢å¤é¨ç½ç»
    	msg.setType(NetworkType.EXTERNAL.getValue());
    	PageResult<ExternalNetworkSetType> result = networkDao.describeNetworks(msg);	
    	for(ExternalNetworkSetType network: result.getList()) {
    		network.setCnStatus(NetworkStatus.getCnName(network.getStatus()));
    		if(StringUtils.isNotBlank(network.getSubnetIds())) {
    			String subnetIdsStr = network.getSubnetIds();
    			String[] subnetArr = subnetIdsStr.split(",");
    			List<String> subnetIds = new ArrayList<String>();
    			for(String subnetInfo:subnetArr) {
    				subnetIds.add(subnetInfo.substring(0, subnetInfo.indexOf("#")));
    			}
    			//List<String> subnetIds = Arrays.asList(subnetIdsStr.split(","));
    			VSwitchIds vSwitchIds = new VSwitchIds();
    			vSwitchIds.setvSwitchId(subnetIds);
    			network.setvSwitchIds(vSwitchIds);
    		}	
    	}
        return result;
    }

    @Override
    public void getNetworks(String id) {
        // TODO Auto-generated method stub

    }

    private INetworkProvider getProviderOrDefault() {
        INetworkProvider provider = ResourceProviders.getDefault(ResourceType.NETWORK);
        return provider;
    }

    private INetworkProvider checkAndGetProvider(Integer providerType) {
        INetworkProvider provider = ResourceProviders.checkAndGet(ResourceType.NETWORK, providerType);
        return provider;
    }

	@Override
	public DetailExternalNetwork detailExternalNetwork(DetailExternalNetworkParams params, CurrentUser currentUser) {
		DetailExternalNetwork response = new DetailExternalNetwork();
		Network network = networkDao.getById(params.getNetworkId());
		if(null == network) {
			return response;
		}
		
		//TODO æ°æ®æ¥æº?
		response.setCidrBlock("");
		response.setNetworkId(network.getId());
		response.setNetworkName(network.getName());
		response.setStatus(network.getStatus());
		response.setCnStatus(NetworkStatus.getCnName(network.getStatus()));
		response.setCreationTime(network.getCreateTime());
		response.setNetworkType(network.getNetworkType());
		response.setPhysicalNetwork(network.getPhysicalNetwork());
		response.setSegmentId(network.getSegmentId());
		response.setUserId(network.getUserId());
		response.setUserName(CacheContainer.getInstance().getString(CacheType.USER_NAME, network.getUserId()));
		
		return response;
	}

	@Override
	public TplExternalNetworkResponse tplExternalNetworks(ApiTplExternalNetworksMsg msg) {
		TplExternalNetworkResponse res = new TplExternalNetworkResponse();
		List<TplExternalNetworkItem> externalNetworkItems = new ArrayList<TplExternalNetworkItem>();
		
		DescribeExternalNetworksMsg params = new DescribeExternalNetworksMsg();
		params.setPageSize(9999);
		params.setCurrentUser(msg.getCurrentUser());
		params.setType(NetworkType.EXTERNAL.getValue());
    	PageResult<ExternalNetworkSetType> result = networkDao.describeNetworks(params);
    	for(ExternalNetworkSetType network: result.getList()) {
    		TplExternalNetworkItem externalNetworkItem = new TplExternalNetworkItem();
    		externalNetworkItem.setNetworkId(network.getNetworkId());
    		externalNetworkItem.setNetworkName(network.getNetworkName());
    		TplVSwitchItems tplItems = new TplVSwitchItems();
    		if(StringUtils.isNotBlank(network.getSubnetIds())) {
    			List<TplVSwitchItem> vSwitchItemItems = new ArrayList<TplVSwitchItem>();
    			String[] subnetArr = network.getSubnetIds().split(",");
    			String subnetIdStr = "";
    			for(String subnetInfo:subnetArr) {
    				String[] subnetInfoArr = subnetInfo.split("#");
    				subnetIdStr += subnetInfoArr[0] + ",";
    				TplVSwitchItem item = new TplVSwitchItem();
    				item.setvSwitchId(subnetInfoArr[0]);
    				item.setvSwitchName(subnetInfoArr[1]);
    				item.setCidrBlock(subnetInfoArr[2]);
    				item.setvRouterId(subnetInfoArr[3]);
    				
    				vSwitchItemItems.add(item);
    			}
    			network.setSubnetIds(subnetIdStr.substring(0, subnetIdStr.length()));
    			tplItems.setvSwitchItemItem(vSwitchItemItems);
    		}
    		externalNetworkItem.setvSwitchItemItems(tplItems);
    		externalNetworkItems.add(externalNetworkItem);
    	}
    	res.setExternalNetwork(externalNetworkItems);
		return res;
	}

}