package com.gcloud.controller.network.entity;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */



@Table(name = "gc_ipallocation_pools", jdbc = "controllerJdbcTemplate")
public class IpallocationPools {

    @ID
    private String id;
    private String subnetId;
    private String firstIp;
    private String lastIp;

    public static final String ID = "id";
    public static final String SUBNET_ID = "subnetId";
    public static final String FIRST_IP = "firstIp";
    public static final String LAST_IP = "lastIp";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubnetId() {
        return subnetId;
    }

    public void setSubnetId(String subnetId) {
        this.subnetId = subnetId;
    }

    public String getFirstIp() {
        return firstIp;
    }

    public void setFirstIp(String firstIp) {
        this.firstIp = firstIp;
    }

    public String getLastIp() {
        return lastIp;
    }

    public void setLastIp(String lastIp) {
        this.lastIp = lastIp;
    }

    public String updateId(String id) {
        this.setId(id);
        return ID;
    }

    public String updateSubnetId(String subnetId) {
        this.setSubnetId(subnetId);
        return SUBNET_ID;
    }

    public String updateFirstIp(String firstIp) {
        this.setFirstIp(firstIp);
        return FIRST_IP;
    }

    public String updateLastIp(String lastIp) {
        this.setLastIp(lastIp);
        return LAST_IP;
    }


}