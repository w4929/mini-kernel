package com.gcloud.controller.network.service.impl;

import com.gcloud.common.util.NetworkUtil;
import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.ResourceProviders;
import com.gcloud.controller.compute.dao.InstanceDao;
import com.gcloud.controller.compute.entity.VmInstance;
import com.gcloud.controller.network.dao.FloatingIpDao;
import com.gcloud.controller.network.dao.IpallocationDao;
import com.gcloud.controller.network.dao.IpallocationPoolDao;
import com.gcloud.controller.network.dao.NetworkDao;
import com.gcloud.controller.network.dao.OvsBridgeDao;
import com.gcloud.controller.network.dao.PortDao;
import com.gcloud.controller.network.dao.QosPortPolicyBindingDao;
import com.gcloud.controller.network.dao.RouterDao;
import com.gcloud.controller.network.dao.SecurityGroupDao;
import com.gcloud.controller.network.dao.SecurityGroupPortBindingDao;
import com.gcloud.controller.network.dao.SubnetDao;
import com.gcloud.controller.network.entity.FloatingIp;
import com.gcloud.controller.network.entity.Ipallocation;
import com.gcloud.controller.network.entity.IpallocationPools;
import com.gcloud.controller.network.entity.Network;
import com.gcloud.controller.network.entity.OvsBridge;
import com.gcloud.controller.network.entity.Port;
import com.gcloud.controller.network.entity.QosPolicy;
import com.gcloud.controller.network.entity.QosPortPolicyBinding;
import com.gcloud.controller.network.entity.Router;
import com.gcloud.controller.network.entity.SecurityGroup;
import com.gcloud.controller.network.entity.SecurityGroupPortBinding;
import com.gcloud.controller.network.entity.Subnet;
import com.gcloud.controller.network.model.CreatePortParams;
import com.gcloud.controller.network.model.DescribeNetworkInterfacesParams;
import com.gcloud.controller.network.model.DetailNetworkInterfaceParams;
import com.gcloud.controller.network.provider.IPortProvider;
import com.gcloud.controller.network.service.IFloatingIpService;
import com.gcloud.controller.network.service.IPortService;
import com.gcloud.controller.network.service.IQosBandwidthLimitRuleService;
import com.gcloud.controller.network.service.IQosPolicyService;
import com.gcloud.controller.network.service.IQosPortPolicyBindingService;
import com.gcloud.controller.utils.UserUtil;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.simpleflow.Flow;
import com.gcloud.core.simpleflow.NoRollbackFlow;
import com.gcloud.core.simpleflow.SimpleFlowChain;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.compute.enums.DeviceOwner;
import com.gcloud.header.compute.msg.node.vm.model.VmNetworkDetail;
import com.gcloud.header.enums.ProviderType;
import com.gcloud.header.enums.ResourceType;
import com.gcloud.header.network.enums.NetworkStatus;
import com.gcloud.header.network.model.DetailNic;
import com.gcloud.header.network.model.NetworkInterfaceSet;
import com.gcloud.header.network.model.NicSecurityGroup;
import com.gcloud.header.network.model.SecurityGroupIdSetType;
import com.gcloud.header.network.msg.api.ApiNetworkInterfaceStatisticsMsg;
import com.gcloud.header.network.msg.api.ApiNetworkInterfaceStatisticsReplyMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Service
@Slf4j
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class PortServiceImpl implements IPortService {

    @Autowired
    private PortDao portDao;

    @Autowired
    private IpallocationDao ipallocationDao;

    @Autowired
    private SubnetDao subnetDao;
    
    @Autowired
    private RouterDao routerDao;

    @Autowired
    private SecurityGroupDao securitygroupDao;

    @Autowired
    private SecurityGroupPortBindingDao securityGroupPortBindingDao;

    @Autowired
    private IQosPolicyService qosPolicyService;

    @Autowired
    private QosPortPolicyBindingDao qosPortPolicyBindingDao;

    @Autowired
    private IQosBandwidthLimitRuleService qosBandwidthLimitRuleService;

    @Autowired
    private IQosPortPolicyBindingService qosPortPolicyBindingService;

    @Autowired
    private FloatingIpDao floatingIpDao;

    @Autowired
    private OvsBridgeDao ovsBridgeDao;
    
    @Autowired
    private IFloatingIpService floatingIpservice;
    
    @Autowired
    private NetworkDao networkDao;
    
    @Autowired
    private InstanceDao instanceDao;

    @Autowired
    private IpallocationPoolDao ipallocationPoolDao;

    @Override
    public PageResult<NetworkInterfaceSet> describe(DescribeNetworkInterfacesParams params, CurrentUser currentUser){

        if(params == null){
            params = new DescribeNetworkInterfacesParams();
        }

        PageResult<NetworkInterfaceSet> pageResult = portDao.describePorts(params, NetworkInterfaceSet.class, currentUser);
        pageResult.getList().forEach(p -> p.setCnStatus(NetworkStatus.getCnName(p.getStatus())));
        List<NetworkInterfaceSet> datas = pageResult.getList();
        List<String> filters = datas.stream().filter(data -> StringUtils.isNotBlank(data.getDeviceId())).map(data -> data.getDeviceId()).distinct().collect(Collectors.toList());
        List<VmInstance> instances = instanceDao.getByIds(filters);
        Map<String, String> instanceMap = instances.stream().collect(Collectors.toMap(VmInstance::getId, VmInstance::getAlias));
        if(datas != null && datas.size() > 0){
            for(NetworkInterfaceSet data : datas){
            	if(instanceMap.containsKey(data.getDeviceId())) {
            		data.setInstanceName(instanceMap.get(data.getDeviceId()));
            	}
                String sgIds = data.getSecurityGroupIdsStr();
                if(StringUtils.isNotBlank(sgIds)){
                    SecurityGroupIdSetType sgType = new SecurityGroupIdSetType();
                    sgType.setSecurityGroupId(Arrays.asList(sgIds.split(",")));
                    data.setSecurityGroupIds(sgType);
                }
            }
        }

        return pageResult;
    }

    @Override
    public VmNetworkDetail getNetworkDetail(String portId) {
        Port port = portDao.getById(portId);

        VmNetworkDetail networkDetail = new VmNetworkDetail();
        networkDetail.setSufId(port.getSufId());
        networkDetail.setAftName(port.getAftName());
        networkDetail.setPreName(port.getPreName());
        networkDetail.setBrName(port.getBrName());
        networkDetail.setMacAddress(port.getMacAddress());
        networkDetail.setDeviceOwner(port.getDeviceOwner());
        networkDetail.setPortId(port.getId());
        networkDetail.setDeviceOwner(DeviceOwner.COMPUTE.getValue());
        networkDetail.setNoArpLimit(port.getNoArpLimit());
        networkDetail.setPortRefId(port.getProviderRefId());
        networkDetail.setPortProvider(port.getProvider());

        //
        Ipallocation ipallocation = ipallocationDao.findUniqueByProperty("portId", port.getId());
        if(ipallocation != null){
            networkDetail.setIp(ipallocation.getIpAddress());
            networkDetail.setSubnetId(ipallocation.getSubnetId());
        }

        if(StringUtils.isNotBlank(port.getOvsBridgeId())){
            OvsBridge ovsBridge = ovsBridgeDao.getById(port.getOvsBridgeId());
            if(ovsBridge == null){
                throw new GCloudException("::获取ovs网桥失败");
            }
            networkDetail.setCustomOvsBr(ovsBridge.getBridge());
            networkDetail.setOvsBridgeId(ovsBridge.getId());
        }

        return networkDetail;
    }


    @Override
    public String create(CreatePortParams params, CurrentUser currentUser){

        Subnet subnet = subnetDao.getById(params.getSubnetId());
        if(subnet == null){
            throw new GCloudException("0080103::找不到对应的子网");
        }
        params.setSubnetRefId(subnet.getProviderRefId());
        params.setNetworkId(subnet.getNetworkId());

        if(StringUtils.isNotBlank(params.getDeviceOwner())){
            DeviceOwner deviceOwner = DeviceOwner.value(params.getDeviceOwner());
            if(deviceOwner == null){
                throw new GCloudException("0080110::不支持此设备类型");
            }
        }

        //启用安全�?
        if(params.getPortSecurityEnabled() == null || params.getPortSecurityEnabled()){

            if(StringUtils.isNotBlank(params.getSecurityGroupId())){
                SecurityGroup securitygroup = securitygroupDao.getById(params.getSecurityGroupId());
                if(securitygroup == null){
                    throw new GCloudException("0080104::找不到对应的安全�?");
                }
                params.setSecurityGroupRefId(securitygroup.getProviderRefId());
            }else {
                SecurityGroup securitygroup = null;
                Map<String, Object> filters = new HashMap<>();
                filters.put(SecurityGroup.TENANT_ID, currentUser.getDefaultTenant());
                filters.put(SecurityGroup.IS_DEFAULT, true);
                securitygroup = securitygroupDao.findUniqueByProperties(filters);
                if(securitygroup == null){
//                filters = new HashMap<>();
//                filters.put(SecurityGroup.TENANT_ID, currentUser.getDefaultTenant());
//                securitygroup = securitygroupDao.findUniqueByProperties(filters);
                    List<SecurityGroup> sgs = securitygroupDao.findByProperty(SecurityGroup.TENANT_ID, currentUser.getDefaultTenant());
                    if (sgs == null && sgs.size() < 1) {
                        throw new GCloudException("0080104::找不到对应的安全�?");
                    }
                    securitygroup = sgs.get(0);
                }
                params.setSecurityGroupId(securitygroup.getId());
                params.setSecurityGroupRefId(securitygroup.getProviderRefId());
            }

        }


        if(StringUtils.isNotBlank(params.getIpAddress())){
            Map<String, Object> portFilters = new HashMap<>();
            portFilters.put(Ipallocation.SUBNET_ID, subnet.getId());
            portFilters.put(Ipallocation.IP_ADDRESS, params.getIpAddress());
            Ipallocation ipallocation = ipallocationDao.findOneByProperties(portFilters);
            if(ipallocation != null){
                throw new GCloudException("0080109::IP已经被分�?");
            }
        }else if(subnet.getEnableDhcp() != null && subnet.getEnableDhcp()){

            if(!hasAvailableIp(subnet.getId())){
                throw new GCloudException("0080111::没有可用的IP");
            }

        }


        return this.getProviderOrDefault(subnet.getProvider()).createPort(params, currentUser);
    }

    public boolean hasAvailableIp(String subnetId){

        List<IpallocationPools> pools = ipallocationPoolDao.findByProperty(IpallocationPools.SUBNET_ID, subnetId);
        //可能不是dhcp，校验无�?
        if(pools == null || pools.size() == 0){
            return true;
        }

        Set<String> availableIps = new HashSet<>();
        pools.stream().forEach(p -> availableIps.addAll(NetworkUtil.rangeIp(p.getFirstIp(), p.getLastIp())));

        List<String> allocateIps = ipallocationDao.subnetIps(subnetId);
        availableIps.removeAll(allocateIps);
        return availableIps.size() > 0;
    }

    @Override
    public void update(String portId, List<String> securityGroupIds, String portName){

        Port port = portDao.getById(portId);
        if(port == null){
            throw new GCloudException("0080202::找不到对应的端口");
        }

        this.checkAndGetProvider(port.getProvider()).updatePort(port, securityGroupIds, portName);
        CacheContainer.getInstance().put(CacheType.PORT_NAME, portId, portName);
    }

    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void delete(String portId) {

        List<QosPortPolicyBinding> bindings = qosPortPolicyBindingDao.findByProperty(QosPortPolicyBinding.PORT_ID, portId);

        deletePort(portId);

        //规则删除失败，无法回滚port，则不会�?
        //不能先删除port再删�? qos，否则qos删除成功后，出现异常，port将没有带宽限�?
        //TODO �?要增加后续处理处理删除失败情况�??
        if(bindings != null && bindings.size() > 0){
            for(QosPortPolicyBinding binding : bindings){
                try{
                    qosPolicyService.delete(binding.getPolicyId());
                }catch (Exception ex){
                    log.error(String.format("删除port qos policy 失败。policy id = %s, ex = %s", binding.getPolicyId(), ex), ex);
                }

            }
        }
    }

    private void deletePort(String portId){
        Port port = portDao.getById(portId);
        if(port == null){
            throw new GCloudException("0080302::找不到对应的端口");
        }

        if(StringUtils.isNotBlank(port.getDeviceId())){
            throw new GCloudException("0080303::端口已经挂载，请卸载后再删除");
        }
        //改为，有绑定就不能删
        List<FloatingIp> fips = floatingIpDao.findByProperty(FloatingIp.FIXED_PORT_ID, portId);
        /*if(fips != null && fips.size() > 0){
            throw new GCloudException("0080304::请先卸载弹�?�公网ip");
        }*/
        if(fips != null && fips.size() > 0){
            fips.stream().forEach(fip -> floatingIpservice.unAssociateEipAddress(fip.getId()));
        }

        this.checkAndGetProvider(port.getProvider()).deletePort(port);
    }

    @Override
    public void updateQos(String portId, Integer egress, Integer ingress) {
        Port port = portDao.getById(portId);
        if(port == null){
            throw new GCloudException("0080502::找不到对应的端口");
        }

        updatePortQos(portId, egress, ingress);
    }

    @Override
    public void updatePortQos(String portId, Integer egress, Integer ingress){
        Port port = portDao.getById(portId);
        //是否已经有策�?
        //neutron port 只支持绑定一个策�?,如果升级neutron版本后，支持多个策略，则�?要修改�?�辑
        QosPortPolicyBinding binding = qosPortPolicyBindingDao.findUniqueByProperty(QosPortPolicyBinding.PORT_ID, portId);
        if(binding == null){
            createQosLimit(port, egress, ingress);
        }else{
            qosPolicyService.updateQosLimit(binding.getPolicyId(), egress, ingress);
        }
    }

    private void createQosLimit(Port port, Integer egress, Integer ingress){

        SimpleFlowChain<QosPolicy, String> chain = new SimpleFlowChain<>("create qos limit");
        chain.then(new Flow<QosPolicy>("create qos policy") {
            @Override
            public void run(SimpleFlowChain chain, QosPolicy data) {
                QosPolicy qosPolicy = qosPolicyService.createQosLimit(port.getProvider(), egress, ingress);
                chain.data(qosPolicy);
                chain.next();
            }
            @Override
            public void rollback(SimpleFlowChain chain, QosPolicy data) {
                qosPolicyService.delete(data.getId());
                chain.rollback();
            }

            //创建规则的时候不会滚，直接又会policy回滚是删�?
        }).then(new NoRollbackFlow<QosPolicy>("bind port and qos policy") {
            @Override
            public void run(SimpleFlowChain chain, QosPolicy data) {
                qosPortPolicyBindingService.bind(port.getId(), data.getId());
                chain.next();
            }
        }).start();

        if(StringUtils.isNotBlank(chain.getErrorCode())){
            throw new GCloudException(chain.getErrorCode());
        }
    }




    @Override
    public void updatePort(Port port) {
        this.checkAndGetProvider(port.getProvider()).updatePort(port);
    }

    @Override
    public void attachPort(VmInstance instance, String portId, String customOvsBr, Boolean noArpLimit) {
        Port port = portDao.getById(portId);
        if(port == null){
            throw new GCloudException("0080502::找不到对应的端口");
        }
        this.checkAndGetProvider(port.getProvider()).attachPort(instance, port, customOvsBr, noArpLimit);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void cleanPortData(String portId) {
        ipallocationDao.deleteByPortId(portId);
        securityGroupPortBindingDao.deleteByPortId(portId);
        qosPortPolicyBindingDao.deleteByPortId(portId);
        portDao.deleteById(portId);

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void createPortData(CreatePortParams params, ProviderType provider, CurrentUser currentUser, String portId, String portRefId, String macAddress, String state, String ipAddress) {
        Port port = new Port();
        port.setId(portId);
        port.setMacAddress(macAddress);
        port.setNetworkId(params.getNetworkId());
        port.setUserId(currentUser.getId());
        port.setTenantId(currentUser.getDefaultTenant());
        port.setDescription(params.getDescription());
        port.setStatus(state);
        
        //port如果没有名字，则将id设置为名�?
        if(StringUtils.isBlank(params.getName())) {
        	port.setName(portId);
        } else {
        	port.setName(params.getName());
        }
        port.setCreateTime(new Date());
        port.setProvider(provider.getValue());
        port.setProviderRefId(portRefId);

        if(StringUtils.isNotBlank(params.getDeviceId())){
            port.setDeviceId(params.getDeviceId());
        }
        if(StringUtils.isNotBlank(params.getDeviceOwner())){
            port.setDeviceOwner(params.getDeviceOwner());
        }

        portDao.save(port);

        if(StringUtils.isNotBlank(ipAddress)){
            Ipallocation ipallocation = new Ipallocation();
            ipallocation.setIpAddress(ipAddress);
            ipallocation.setPortId(port.getId());
            ipallocation.setSubnetId(params.getSubnetId());
            ipallocation.setNetworkId(port.getNetworkId());
            ipallocationDao.save(ipallocation);
        }

        //启用安全�?
        if(params.getPortSecurityEnabled() == null || params.getPortSecurityEnabled()){
            SecurityGroupPortBinding bind = new SecurityGroupPortBinding();
            bind.setPortId(port.getId());
            bind.setSecurityGroupId(params.getSecurityGroupId());

            securityGroupPortBindingDao.save(bind);
        }
    }

    private IPortProvider getProviderOrDefault(Integer providerType) {
        IPortProvider provider = ResourceProviders.getOrDefault(ResourceType.PORT, providerType);
        return provider;
    }
    
    private IPortProvider checkAndGetProvider(Integer providerType) {
        IPortProvider provider = ResourceProviders.checkAndGet(ResourceType.PORT, providerType);
        return provider;
    }

    @Override
    public void detachDone(Port port) {
        Port updatePort = new Port();
        updatePort.setId(port.getId());
        List<String> updateField = new ArrayList<>();

        updateField.add(updatePort.updateSufId(""));
        updateField.add(updatePort.updateAftName(""));
        updateField.add(updatePort.updatePreName(""));
        updateField.add(updatePort.updateBrName(""));
        updateField.add(updatePort.updateDeviceOwner(""));
        updateField.add(updatePort.updateDeviceId(""));
        updateField.add(updatePort.updateOvsBridgeId(""));

        portDao.update(updatePort, updateField);
        checkAndGetProvider(port.getProvider()).detachDone(port);

    }

	@Override
	public DetailNic detailNetworkInterface(DetailNetworkInterfaceParams params, CurrentUser currentUser) {
		DetailNic response = new DetailNic();
		Port port = portDao.getById(params.getNetworkInterfaceId());
		if(null == port) {
			return response;
		}
		
		//网卡的基�?信息
		response.setCreationTime(port.getCreateTime());
		response.setMacAddress(port.getMacAddress());
		response.setNetworkInterfaceId(port.getId());
		response.setNetworkInterfaceName(port.getName());
		response.setStatus(port.getStatus());
		response.setCnStatus(NetworkStatus.getCnName(port.getStatus()));
        response.setProvider(port.getProvider());
        response.setProviderRefId(port.getProviderRefId());
		
		//创建者信息处�?
		String userName = UserUtil.userName(port.getUserId());
		response.setCreator(userName);
		
		//初始化ip地址信息
		Map<String, Object> ipAllocParams = new HashMap<>();
		ipAllocParams.put(Ipallocation.PORT_ID, params.getNetworkInterfaceId());
		List<Ipallocation> ipList = ipallocationDao.findByProperties(ipAllocParams);
		Ipallocation ip = null;
		if(ipList != null && !ipList.isEmpty()) {
			ip = ipList.get(0);
			response.setPrivateIpAddress(ip.getIpAddress());
		}

		//处理网卡详情的owner
		String deviceId = port.getDeviceId();
		String deviceType = port.getDeviceOwner();
		response.setDeviceId(deviceId);
		response.setDeviceType(deviceType);
		if(DeviceOwner.COMPUTE.getValue().equalsIgnoreCase(deviceType)) {
			VmInstance vm = instanceDao.getById(deviceId);
			if(null != vm) {
				response.setDeviceName(vm.getAlias());
			}
		}
		else if(DeviceOwner.ROUTER.getValue().equalsIgnoreCase(deviceType) || DeviceOwner.HA_ROUTER.getValue().equalsIgnoreCase(deviceType)) {
			Router router = routerDao.getById(deviceId);
			if(null != router) {
				response.setDeviceName(router.getName());
			}
		}
		//TODO 其他device的数据处�?
		
		//有关networkId的信息的处理
		String networkId = port.getNetworkId();
		response.setNetworkId(networkId);
		if(StringUtils.isNotBlank(networkId)) {
			//vpc信息和networkName
			Network network = networkDao.getById(port.getNetworkId());
			String networkName = network.getName();
			String pattern = "vpcId:(\\S+)-network";
			Pattern p = Pattern.compile(pattern);
			Matcher m = p.matcher(networkName);
			if(m.find()) {
				String vpcId = m.group(1);
				Router router = routerDao.getById(vpcId);
				response.setVpcId(router.getId());
				response.setVpcName(router.getName());
				response.setNetworkName(router.getName());
			} else {
				response.setNetworkName(networkName);
			}
			
		}
		
		//vswitch信息的处�?
		if(null != ip) {
			String subnetId = ip.getSubnetId();
			Subnet subnet = subnetDao.getById(subnetId);
			if(null != subnet) {
				response.setvSwitchId(subnet.getId());
				response.setvSwitchName(subnet.getName());
			}
		}
		
		//安全组信息处�?
		Map<String, Object> portSgParams = new HashMap<>();
		portSgParams.put(SecurityGroupPortBinding.PORT_ID, params.getNetworkInterfaceId());
		List<NicSecurityGroup> securityGroupList = securityGroupPortBindingDao.findInfosByPortId(params.getNetworkInterfaceId(), NicSecurityGroup.class);
		response.setSecurityGroups(securityGroupList);
		
		//TODO 网卡没发现有可用区信�?
//		response.setZoneId("");
//		response.setZoneName("");
		return response;
	}

	@Override
	public ApiNetworkInterfaceStatisticsReplyMsg statistic(ApiNetworkInterfaceStatisticsMsg msg) {
		ApiNetworkInterfaceStatisticsReplyMsg reply = new ApiNetworkInterfaceStatisticsReplyMsg();
		reply.setAllNum(portDao.statisticPort(msg));
		return reply;
	}

    @Override
    public void migratePort(String portId, String sourceHostName, String targetHostname) {

        Port port = portDao.getById(portId);
        if(port == null){
            throw new GCloudException("::找不到对应的端口");
        }

        this.checkAndGetProvider(port.getProvider()).migratePort(port.getProviderRefId(), sourceHostName, targetHostname);
        //数据库没有记录，不需要修�?
//        List<String> updateFields = new ArrayList<>();
//        updateFields.addAll(Port.)

    }


    @Override
    public void migratePortIgnoreBinding(String portId, String sourceHostName, String targetHostname) {

        Port port = portDao.getById(portId);
        if(port == null){
            throw new GCloudException("::找不到对应的端口");
        }

        this.checkAndGetProvider(port.getProvider()).migratePortIgnoreBinding(port.getProviderRefId(), sourceHostName, targetHostname);
        //数据库没有记录，不需要修�?
//        List<String> updateFields = new ArrayList<>();
//        updateFields.addAll(Port.)

    }
}