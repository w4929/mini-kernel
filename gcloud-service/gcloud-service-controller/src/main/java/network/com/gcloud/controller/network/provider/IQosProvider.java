package com.gcloud.controller.network.provider;

import com.gcloud.controller.IResourceProvider;
import com.gcloud.header.compute.enums.QosDirection;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public interface IQosProvider extends IResourceProvider {

    String createQosPolicy(String name, String description, Boolean isDefault, Boolean shared);

    void deleteQosPolicy(String refId);

    String createQosBandwidthLimitRule(String policyRefId, Integer maxKbps, Integer maxBurstKbps, QosDirection qosDirection);

    void updateQosBandwidthLimitRule(String policyRefId, String ruleRefId, Integer maxKbps, Integer maxBurstKbps, QosDirection qosDirection);

    void deleteQosBandwidthLimitRule(String policyRefId, String ruleRefId);

    void bindPort(String portRefId, String policyRefId);

    void bindFip(String fipRefId, String policyRefId);

}