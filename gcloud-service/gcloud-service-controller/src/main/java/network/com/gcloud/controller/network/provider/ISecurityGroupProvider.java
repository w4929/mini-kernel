package com.gcloud.controller.network.provider;

import com.gcloud.controller.IResourceProvider;
import com.gcloud.controller.network.entity.SecurityGroup;
import com.gcloud.controller.network.model.AuthorizeSecurityGroupParams;
import com.gcloud.controller.network.model.CreateSecurityGroupParams;
import com.gcloud.controller.network.model.ModifySecurityGroupAttributeParams;
import com.gcloud.header.api.model.CurrentUser;

import java.util.List;
import java.util.Map;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public interface ISecurityGroupProvider extends IResourceProvider {

    String createSecurityGroup(CreateSecurityGroupParams params, CurrentUser currentUser);

    void deleteSecurityGroup(String id);

    void deleteSecurityGroup(SecurityGroup securityGroup);

    void modifySecurityGroup(ModifySecurityGroupAttributeParams params);

    void modifySecurityGroup(ModifySecurityGroupAttributeParams params, SecurityGroup sg);

    String authorizeSecurityGroup(AuthorizeSecurityGroupParams params);

    void revokeSecurityGroup(String securityGroupId, String securityGroupRuleId);

//    DescribeSecurityGroupAttributeResponse describeSecurityGroupAttribute(String securityGroupId, String direction, String regionId);

    List<SecurityGroup> list(Map<String, String> filter);

}