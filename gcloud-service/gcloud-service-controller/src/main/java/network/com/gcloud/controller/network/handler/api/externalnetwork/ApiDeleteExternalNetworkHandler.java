package com.gcloud.controller.network.handler.api.externalnetwork;

import com.gcloud.controller.network.service.INetworkService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.msg.api.DeleteExternalNetworkMsg;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@GcLog(taskExpect="删除外网")
@ApiHandler(module=Module.ECS, subModule=SubModule.NETWORK, action="DeleteExternalNetwork",name="删除外网")

public class ApiDeleteExternalNetworkHandler extends MessageHandler<DeleteExternalNetworkMsg, ApiReplyMessage>{

	@Autowired
	INetworkService service;
	
	@Override
	public ApiReplyMessage handle(DeleteExternalNetworkMsg msg) throws GCloudException {
		service.removeNetwork(msg.getNetworkId());
		msg.setObjectId(msg.getNetworkId());
		msg.setObjectName(CacheContainer.getInstance().getString(CacheType.NETWORK_NAME, msg.getNetworkId()));
		return new ApiReplyMessage();
	}

}