package com.gcloud.controller.network.provider.enums.neutron;


import com.gcloud.header.compute.enums.SecurityDirection;

import java.util.Arrays;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum NeutronSecurityDirection {
    EGRESS("egress", SecurityDirection.EGRESS),
    INGRESS("ingress", SecurityDirection.INGRESS);

    NeutronSecurityDirection(String neutronSecurityDirection, SecurityDirection securityDirection) {
        this.neutronSecurityDirection = neutronSecurityDirection;
        this.securityDirection = securityDirection;
    }

    private String neutronSecurityDirection;
    private SecurityDirection securityDirection;

    public String getNeutronSecurityDirection() {
        return neutronSecurityDirection;
    }

    public SecurityDirection getSecurityDirection() {
        return securityDirection;
    }

    public static String toGCloudValue(String neutronValue){
        NeutronSecurityDirection direction = Arrays.stream(NeutronSecurityDirection.values()).filter(o -> o.getNeutronSecurityDirection().equals(neutronValue)).findFirst().orElse(null);
        return direction == null ? null : direction.getSecurityDirection().value();
    }

    public static String getByGcValue(String gcValue){
        NeutronSecurityDirection direction = Arrays.stream(NeutronSecurityDirection.values()).filter(o -> o.getSecurityDirection().value().equals(gcValue)).findFirst().orElse(null);
        return direction == null ? null : direction.getNeutronSecurityDirection();
    }
}