package com.gcloud.controller.compute.handler.api.vm.senior;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.compute.model.vm.DescribeMigrateNodesParams;
import com.gcloud.controller.compute.service.vm.senior.IVmSeniorService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.vm.senior.ApiDescribeMigrateNodesMsg;
import com.gcloud.header.compute.msg.api.vm.senior.ApiDescribeMigrateNodesReplyMsg;
import com.gcloud.header.compute.msg.node.node.model.NodeBaseInfo;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module = Module.ECS, subModule=SubModule.VM, action = "DescribeMigrateNodes", name = "迁移节点列表")
public class ApiDescribeMigrateNodesHandler extends MessageHandler<ApiDescribeMigrateNodesMsg, ApiDescribeMigrateNodesReplyMsg>{

	@Autowired
	private IVmSeniorService service;
	
	@Override
	public ApiDescribeMigrateNodesReplyMsg handle(ApiDescribeMigrateNodesMsg msg) throws GCloudException {
		DescribeMigrateNodesParams params = BeanUtil.copyProperties(msg, DescribeMigrateNodesParams.class);
		
		PageResult<NodeBaseInfo> response = service.describeMigrateNodes(params, msg.getCurrentUser());
		ApiDescribeMigrateNodesReplyMsg reply = new ApiDescribeMigrateNodesReplyMsg();
		reply.init(response);
		return reply;
	}

}