package com.gcloud.controller.compute.service.vm.senior.impl;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.compute.dao.ComputeNodeDao;
import com.gcloud.controller.compute.dao.InstanceDao;
import com.gcloud.controller.compute.dao.InstanceTypeDao;
import com.gcloud.controller.compute.dispatcher.Dispatcher;
import com.gcloud.controller.compute.entity.InstanceType;
import com.gcloud.controller.compute.entity.VmInstance;
import com.gcloud.controller.compute.model.node.AvailableVmResource;
import com.gcloud.controller.compute.model.node.Node;
import com.gcloud.controller.compute.model.vm.DescribeMigrateNodesParams;
import com.gcloud.controller.compute.model.vm.VmBundleResponse;
import com.gcloud.controller.compute.service.vm.senior.IVmSeniorService;
import com.gcloud.controller.compute.utils.RedisNodesUtil;
import com.gcloud.controller.image.dao.ImageDao;
import com.gcloud.controller.image.dao.ImagePropertyDao;
import com.gcloud.controller.image.dao.VmIsoAttachmentDao;
import com.gcloud.controller.image.entity.Image;
import com.gcloud.controller.image.entity.ImageProperty;
import com.gcloud.controller.image.entity.VmIsoAttachment;
import com.gcloud.controller.image.entity.enums.ImagePropertyItem;
import com.gcloud.controller.image.service.IImageService;
import com.gcloud.controller.storage.dao.VolumeDao;
import com.gcloud.controller.utils.UserUtil;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.compute.enums.ComputeNodeState;
import com.gcloud.header.compute.enums.ImageOwnerType;
import com.gcloud.header.compute.enums.VmState;
import com.gcloud.header.compute.enums.VmTaskState;
import com.gcloud.header.compute.msg.node.node.model.NodeBaseInfo;
import com.gcloud.header.compute.msg.node.vm.base.JoinDomainMsg;
import com.gcloud.header.compute.msg.node.vm.base.MoveDomainMsg;
import com.gcloud.header.compute.msg.node.vm.base.RemoveDomainMsg;
import com.gcloud.header.identity.ldap.GetLdapConfReplyMsg;
import com.gcloud.header.image.enums.ImageStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Map;
import java.util.UUID;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Service
@Slf4j
@Transactional
public class VmSeniorServiceImpl implements IVmSeniorService {

    @Autowired
    private InstanceDao instanceDao;

    @Autowired
    private InstanceTypeDao instanceTypeDao;

    @Autowired
    private ImageDao imageDao;

    @Autowired
    private IImageService imageService;

    @Autowired
    private ImagePropertyDao imagePropertyDao;

    @Autowired
    private VmIsoAttachmentDao vmIsoAttachmentDao;
    
    @Autowired
    private MessageBus bus;
    
    @Autowired
    private ComputeNodeDao nodeDao;

    @Autowired
    private VolumeDao volumeDao;
    
    @Override
    public VmBundleResponse bundle(String instanceId, String imageName, boolean inTask, CurrentUser currentUser) {

        VmInstance vm = instanceDao.getById(instanceId);
        if (vm == null) {
            throw new GCloudException("0011202::云服务器不能存在");
        }

        if(!VmState.STOPPED.value().equals(vm.getState())){
            throw new GCloudException("0011205::请先关闭云服务器");
        }

        if(!inTask){
            if (!instanceDao.updateInstanceTaskState(instanceId, VmTaskState.BUNDLING)) {
                throw new GCloudException("0011203::云服务器当前状�?�不能创建镜�?");
            }
        }

        String imageId = UUID.randomUUID().toString();

        Image image = new Image();
        image.setId(imageId);
        image.setCreatedAt(new Date());
        image.setName(imageName);
        image.setMinDisk(0L);
        image.setStatus(ImageStatus.QUEUED.value());
        image.setOwner(currentUser.getId());
        image.setTenantId(currentUser.getDefaultTenant());

        //暂时默认都是公共
        image.setOwnerType(ImageOwnerType.SYSTEM.value());
        image.setDisable(false);
        imageDao.save(image);

        Map<String, String> imageProperties = imageService.getImageProperties(vm.getImageId());
        if(imageProperties != null){

            for(Map.Entry<String, String> props : imageProperties.entrySet()){
                if(ImagePropertyItem.ARCHITECTURE.value().equals(props.getKey()) || ImagePropertyItem.OS_TYPE.value().equals(props.getKey())){
                    ImageProperty property = new ImageProperty();
                    property.setImageId(image.getId());
                    property.setName(props.getKey());
                    property.setValue(props.getValue());

                    imagePropertyDao.save(property);
                }

            }

        }

        VmBundleResponse response = new VmBundleResponse();
        response.setImageId(imageId);
        response.setImageName(imageName);

        return response;
    }

    @Override
    public void modifyInstanceInit(String instanceId, String instanceType, boolean inTask) {
        VmInstance instance = instanceDao.getById(instanceId);
        if (instance == null) {
            throw new GCloudException("0011303::云服务器不存�?");
        }

        InstanceType insType = instanceTypeDao.getById(instanceType);
        if (insType == null) {
            throw new GCloudException("0011304::计算规格不存�?");
        }

        //关机才能修改配置，功能更加稳定，否则如果虚拟机无法�?�过libvirt命令关机，则会超�?
        //关机才能配置，所以不�?要处理资�?
        if(!VmState.STOPPED.value().equals(instance.getState())){
            throw new GCloudException("0011305::请先关闭云服务器");
        }

        if(!inTask){
            if (!instanceDao.updateInstanceTaskState(instanceId, VmTaskState.MODIFYING_CONFIG)) {
                throw new GCloudException("0011306::云服务器当前状�?�不能修改计算规�?");
            }
        }
    }
    
    public void joinDomain(String instanceId,String domain) {
    	GetLdapConfReplyMsg getLdapConfReply=null;
    	if(StringUtils.isNotBlank(domain)) {
    		getLdapConfReply= UserUtil.getLdapConf(domain);
    		if(getLdapConfReply==null) {
    			throw new GCloudException("0011307::获取域服务信息失�?");
    		}
    	}

		VmInstance vm =instanceDao.getById(instanceId);
		GetLdapConfReplyMsg oldLdapConfReply=null;
		if(StringUtils.isNotBlank(vm.getDomain())) {
			if(!vm.getDomain().equals(domain)) {
				oldLdapConfReply= UserUtil.getLdapConf(vm.getDomain());
			}
		}
		if(getLdapConfReply !=null) {
			if(oldLdapConfReply==null) {//入域
				JoinDomainMsg msg=new JoinDomainMsg();
				msg.setInstanceId(instanceId);
				msg.setDomain(getLdapConfReply.getDomain());
				msg.setUser(getLdapConfReply.getUser());
				msg.setPassword(getLdapConfReply.getPassword());
				bus.send(msg);
			}else {//出域入域(变更�?)
				MoveDomainMsg msg=new MoveDomainMsg();
				msg.setInstanceId(instanceId);
				msg.setOldDoamin(oldLdapConfReply.getDomain());
				msg.setOldUser(oldLdapConfReply.getUser());
				msg.setOldPassword(oldLdapConfReply.getPassword());
				msg.setDomain(getLdapConfReply.getDomain());
				msg.setUser(getLdapConfReply.getUser());
				msg.setPassword(getLdapConfReply.getPassword());
				bus.send(msg);
			}
		}else {
			if(oldLdapConfReply!=null) {//出域
				RemoveDomainMsg msg=new RemoveDomainMsg();
				msg.setInstanceId(instanceId);
				msg.setDomain(getLdapConfReply.getDomain());
				msg.setUser(getLdapConfReply.getUser());
				msg.setPassword(getLdapConfReply.getPassword());
				bus.send(msg);
			}
		}
	}

    @Override
    public VmInstance migrateInit(String instanceId, String targetHostName) {
        VmInstance instance = instanceDao.getById(instanceId);
        if(null == instance) {
            log.error("::�?要迁移的云服务器不存�?");
            throw new GCloudException("::�?要迁移的云服务器不存�?");
        }

        //返回原来的instanceId和hostName，给回滚等操作提供数�?
        String resStatus = instance.getState();

        //
        Node sourceNode = RedisNodesUtil.getComputeNodeByHostName(instance.getHostname());
        if(null == sourceNode) {
            log.error("::找不到源节点");
            throw new GCloudException("::找不到源节点");
        }

        if(sourceNode.getHostName().equals(targetHostName)) {
            log.error("::不可迁移到同�?个节�?");
            throw new GCloudException("::不可迁移到同�?个节�?");
        }
        
        Node targetNode = RedisNodesUtil.getComputeNodeByHostName(targetHostName);
        if(null == targetNode) {
        	log.error("::找不到目标节�?");
        	throw new GCloudException("::找不到目标节�?");
        }
        if(!sourceNode.getZoneId().equals(targetNode.getZoneId())) {
        	log.error("::不可跨可用区进行迁移");
        	throw new GCloudException("::不可跨可用区进行迁移");
        }
        
        
        if(!VmState.RUNNING.value().equals(resStatus) && !VmState.STOPPED.value().equals(resStatus)) {
            log.error("::云服务器当前状�?�不可以进行迁移");
            throw new GCloudException("::云服务器当前状�?�不可以进行迁移");
        }

        if (!instanceDao.updateInstanceMigrateState(instanceId, targetHostName)) {
            throw new GCloudException("0011203::云服务器当前状�?�不能创建镜�?");
        }

        //暂时有iso先不允许迁移
        VmIsoAttachment isoAttachments = vmIsoAttachmentDao.findOneByProperty(VmIsoAttachment.INSTANCE_ID, instance.getId());
        if(isoAttachments != null){
            throw new GCloudException("::请先卸载ISO");
        }

        if(volumeDao.instanceHasLocalVolume(instanceId)){
            throw new GCloudException("::云服务器挂载�?本地存储的磁盘，不能迁移");
        }

        //资源占用�?定要放到�?�?
        if(VmState.RUNNING.value().equals(resStatus)){
            Dispatcher.dispatcher().assignNode(targetHostName, instance.getCore(), instance.getMemory());
        }


        return instance;
    }

	@Override
	public PageResult<NodeBaseInfo> describeMigrateNodes(DescribeMigrateNodesParams params, CurrentUser currentUser) {
		PageResult<NodeBaseInfo> page = nodeDao.pageMigrateNodes(params, NodeBaseInfo.class);
		if(page != null && page.getList() != null && page.getList().size() > 0){
		    for(NodeBaseInfo nodeInfo : page.getList()){
		    	nodeInfo.setCnState(ComputeNodeState.getCnName(nodeInfo.getState()));
                Node node = RedisNodesUtil.getComputeNodeByHostName(nodeInfo.getHostName());
                if(node != null){
                    AvailableVmResource resource = node.getAvailableVmResource();
                    nodeInfo.setCpuTotal(resource.getMaxCore());
                    nodeInfo.setCpuAvail(resource.getAvailableCore());
                    nodeInfo.setMemoryTotal(resource.getMaxMemory());
                    nodeInfo.setMemoryAvail(resource.getAvailableMemory());
                }

            }
        }
		
		return page;
	}
}