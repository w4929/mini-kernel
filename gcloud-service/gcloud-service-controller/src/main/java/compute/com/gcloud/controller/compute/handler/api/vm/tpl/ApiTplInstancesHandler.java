package com.gcloud.controller.compute.handler.api.vm.tpl;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.compute.service.vm.base.IVmBaseService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.vm.tpl.ApiTplInstancesMsg;
import com.gcloud.header.compute.msg.api.vm.tpl.ApiTplInstancesReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module = Module.ECS,subModule=SubModule.VM,action = "TplInstances", name = "网络拓扑-云服务器列表")

public class ApiTplInstancesHandler extends MessageHandler<ApiTplInstancesMsg, ApiTplInstancesReplyMsg>{
	@Autowired
	private IVmBaseService vmBaseService;

	@Override
	public ApiTplInstancesReplyMsg handle(ApiTplInstancesMsg msg) throws GCloudException {
		ApiTplInstancesReplyMsg reply = new ApiTplInstancesReplyMsg();
		reply.setInstances(vmBaseService.tplInstances(msg));
		return reply;
	}
}