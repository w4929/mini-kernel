package com.gcloud.controller.compute.handler.api.vm.base;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class Test {

    public static void main(String[] args) {
//        ApiDescribeInstancesMsg msg = new ApiDescribeInstancesMsg();
////        msg.setPageNumber(2);
////
////        CurrentUser currentUser = new CurrentUser();
////        currentUser.setUserTenants(Arrays.asList("a", "b"));
////        msg.setCurrentUser(currentUser);
//
//        DescribeInstancesParams params = BeanUtil.copyProperties(msg, DescribeInstancesParams.class);
//
//        System.out.println(JSON.toJSONString(params, SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue));




        try {
            Enumeration<NetworkInterface> interfaces=null;
            interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) {
                NetworkInterface ni = interfaces.nextElement();
                Enumeration<InetAddress> addresss = ni.getInetAddresses();
                while(addresss.hasMoreElements()){
                    InetAddress nextElement = addresss.nextElement();
                    String hostAddress = nextElement.getHostAddress();
                    System.out.println("本机IP地址为：" +hostAddress);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}