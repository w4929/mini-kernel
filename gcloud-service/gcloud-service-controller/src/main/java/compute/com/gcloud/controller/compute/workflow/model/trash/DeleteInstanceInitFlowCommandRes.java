package com.gcloud.controller.compute.workflow.model.trash;

import java.util.List;

import com.gcloud.core.workflow.model.WorkflowFirstStepResException;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class DeleteInstanceInitFlowCommandRes extends WorkflowFirstStepResException{

    private String vmUserId;
    private List<DetachAndDeleteDiskInfo> disks;
    private List<DetachAndDeleteNetcardInfo> netcards;
    private String isoId;
    private boolean inTask;

    private String taskId;

    public boolean isInTask() {
		return inTask;
	}

	public void setInTask(boolean inTask) {
		this.inTask = inTask;
	}

	public String getIsoId() {
		return isoId;
	}

	public void setIsoId(String isoId) {
		this.isoId = isoId;
	}

	public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public List<DetachAndDeleteDiskInfo> getDisks() {
        return disks;
    }

    public void setDisks(List<DetachAndDeleteDiskInfo> disks) {
        this.disks = disks;
    }

    public List<DetachAndDeleteNetcardInfo> getNetcards() {
        return netcards;
    }

    public void setNetcards(List<DetachAndDeleteNetcardInfo> netcards) {
        this.netcards = netcards;
    }

    public String getVmUserId() {
        return vmUserId;
    }

    public void setVmUserId(String vmUserId) {
        this.vmUserId = vmUserId;
    }
}