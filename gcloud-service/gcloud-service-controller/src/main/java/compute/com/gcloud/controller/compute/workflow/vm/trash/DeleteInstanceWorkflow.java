package com.gcloud.controller.compute.workflow.vm.trash;

import com.gcloud.controller.compute.dao.InstanceDao;
import com.gcloud.controller.compute.entity.VmInstance;
import com.gcloud.controller.compute.service.vm.trash.IVmTrashService;
import com.gcloud.controller.compute.workflow.model.trash.DeleteInstanceWorkflowReq;
import com.gcloud.core.workflow.core.BaseWorkFlows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Component
@Scope("prototype")
@Slf4j
public class DeleteInstanceWorkflow extends BaseWorkFlows {

    @Autowired
    private IVmTrashService vmTrashService;

    @Autowired
    private InstanceDao instanceDao;

    @Override
    public String getFlowTypeCode() {
        return "deleteInstanceWorkflow";
    }

    @Override
    public Object preProcess() {
        return null;
    }

    @Override
    public void process() {

    }

    @Override
    public boolean judgeExecute() {
        DeleteInstanceWorkflowReq req = (DeleteInstanceWorkflowReq)getReqParams();
        if(req.getDeleteNotExist() != null && req.getDeleteNotExist()){
            VmInstance instance = instanceDao.getById(req.getInstanceId());
            if(instance == null){
                return false;
            }
        }
        return true;
    }

    @Override
    protected Class<?> getReqParamClass() {
        return DeleteInstanceWorkflowReq.class;
    }
}