package com.gcloud.controller.compute.workflow.vm.senior.migrate;

import com.gcloud.controller.compute.workflow.model.senior.migrate.MigrateAttachNetworkFlowCommandReq;
import com.gcloud.controller.compute.workflow.model.senior.migrate.MigrateAttachNetworkFlowCommandRes;
import com.gcloud.controller.network.service.IPortService;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
import com.gcloud.header.compute.msg.node.vm.model.VmNetworkDetail;
import com.gcloud.header.compute.msg.node.vm.senior.migrate.MigrateAttachNetworkMsg;
import com.gcloud.header.compute.msg.node.vm.senior.migrate.MigrateDettachNetworkMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@Scope("prototype")
@Slf4j
public class MigrateAttachNetworkFlowCommand extends BaseWorkFlowCommand{
	
	@Autowired
	private IPortService portService;
	
	@Autowired
    private MessageBus bus;
	
	@Override
	protected Object process() throws Exception {
		log.debug("MigrateAttachNetworkFlowCommand start");
		MigrateAttachNetworkFlowCommandReq req = (MigrateAttachNetworkFlowCommandReq) getReqParams();

		VmNetworkDetail networkDetail = portService.getNetworkDetail(req.getNetcardInfo().getPortId());

		//将要创建的网络环境，传出去，方便rollback
		MigrateAttachNetworkFlowCommandRes res = new MigrateAttachNetworkFlowCommandRes();
		res.setVmNetworkDetail(networkDetail);
		
		//通过MQ将数据发送到目标节点，进行迁移前网络的准�?
		MigrateAttachNetworkMsg msg = new MigrateAttachNetworkMsg();
		msg.setInstanceId(req.getInstanceId());
		msg.setVmNetworkDetail(networkDetail);
		msg.setServiceId(MessageUtil.computeServiceId(req.getTargetHostName()));
		msg.setTaskId(getTaskId());
		bus.send(msg);
		log.debug("MigrateAttachNetworkFlowCommand end");
		return res;
	}

	@Override
	protected Object rollback() throws Exception {
		log.debug("MigrateAttachNetworkFlowCommand rollback start");
		System.out.println(this.getClass().getSimpleName() + " rollback()");
		MigrateAttachNetworkFlowCommandReq req = (MigrateAttachNetworkFlowCommandReq) getReqParams();
		MigrateAttachNetworkFlowCommandRes res = (MigrateAttachNetworkFlowCommandRes) getResParams();
		
		//将信息传到目标节�?
		MigrateDettachNetworkMsg msg = new MigrateDettachNetworkMsg();
		msg.setInstanceId(req.getInstanceId());
		msg.setVmNetworkDetail(res.getVmNetworkDetail());
		msg.setServiceId(MessageUtil.computeServiceId(req.getTargetHostName()));
		msg.setTaskId(getTaskId());
		bus.send(msg);
		log.debug("MigrateAttachNetworkFlowCommand rollback start");
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return MigrateAttachNetworkFlowCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return MigrateAttachNetworkFlowCommandRes.class;
	}
}