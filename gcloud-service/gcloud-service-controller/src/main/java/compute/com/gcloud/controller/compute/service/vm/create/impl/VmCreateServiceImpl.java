package com.gcloud.controller.compute.service.vm.create.impl;

import com.gcloud.common.util.NetworkUtil;
import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.compute.dao.ComputeNodeDao;
import com.gcloud.controller.compute.dao.InstanceDao;
import com.gcloud.controller.compute.dao.InstanceTypeDao;
import com.gcloud.controller.compute.dao.ZoneDao;
import com.gcloud.controller.compute.dao.ZoneInstanceTypeDao;
import com.gcloud.controller.compute.dispatcher.Dispatcher;
import com.gcloud.controller.compute.entity.AvailableZoneEntity;
import com.gcloud.controller.compute.entity.ComputeNode;
import com.gcloud.controller.compute.entity.InstanceType;
import com.gcloud.controller.compute.entity.VmInstance;
import com.gcloud.controller.compute.entity.ZoneInstanceTypeEntity;
import com.gcloud.controller.compute.model.node.Node;
import com.gcloud.controller.compute.model.vm.CreateInstanceByImageInitParams;
import com.gcloud.controller.compute.model.vm.CreateInstanceByImageInitResponse;
import com.gcloud.controller.compute.model.vm.VmImageInfo;
import com.gcloud.controller.compute.service.vm.create.IVmCreateService;
import com.gcloud.controller.compute.utils.RedisNodesUtil;
import com.gcloud.controller.compute.utils.VmControllerUtil;
import com.gcloud.controller.image.dao.ImageDao;
import com.gcloud.controller.image.dao.ImagePropertyDao;
import com.gcloud.controller.image.dao.IsoDao;
import com.gcloud.controller.image.entity.Image;
import com.gcloud.controller.image.entity.ImageProperty;
import com.gcloud.controller.image.entity.Iso;
import com.gcloud.controller.network.dao.IpallocationDao;
import com.gcloud.controller.network.dao.SubnetDao;
import com.gcloud.controller.network.entity.Ipallocation;
import com.gcloud.controller.network.entity.Subnet;
import com.gcloud.controller.network.service.IPortService;
import com.gcloud.controller.storage.dao.DiskCategoryDao;
import com.gcloud.controller.storage.dao.StoragePoolDao;
import com.gcloud.controller.storage.entity.DiskCategory;
import com.gcloud.controller.storage.entity.StoragePool;
import com.gcloud.controller.storage.service.IStoragePoolService;
import com.gcloud.controller.storage.service.IVolumeService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.util.ErrorCodeUtil;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.compute.enums.BooleanType;
import com.gcloud.header.compute.enums.CreateType;
import com.gcloud.header.compute.enums.StorageType;
import com.gcloud.header.compute.enums.UseDepartmentType;
import com.gcloud.header.compute.enums.VmState;
import com.gcloud.header.compute.enums.VmTaskState;
import com.gcloud.header.compute.msg.api.model.DiskInfo;
import com.gcloud.header.storage.enums.StoragePoolDriver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */



@Service
@Transactional
@Slf4j
public class VmCreateServiceImpl implements IVmCreateService {
    @Autowired
    private InstanceDao vmInstanceDao;

    @Autowired
    private StoragePoolDao storagePoolDao;

    @Autowired
    private ImageDao imageDao;

    @Autowired
    private ImagePropertyDao imagePropertyDao;

    @Autowired
    private IsoDao isoDao;

    @Autowired
    private SubnetDao subnetDao;

    @Autowired
    private InstanceTypeDao instanceTypeDao;

    @Autowired
    private ComputeNodeDao computeNodeDao;

    @Autowired
    private DiskCategoryDao diskCategoryDao;

    @Autowired
    private IVolumeService volumeService;

    @Autowired
    private ZoneInstanceTypeDao zoneInstanceTypeDao;

    @Autowired
    private ZoneDao zoneDao;

    @Autowired
    private IpallocationDao ipallocationDao;

    @Autowired
    private IStoragePoolService storagePoolService;

    @Autowired
    private IPortService portService;

    @Override
    public CreateInstanceByImageInitResponse createInstanceInit(CreateInstanceByImageInitParams params, CurrentUser currentUser) {
        int createSourceSize = 0;
        String platform = "";
        if (StringUtils.isNotBlank(params.getImageId())) {
            Image image = imageDao.getById(params.getImageId());
            if (image == null) {
                throw new GCloudException("compute_controller_vm_011005::can not find the image");
            }

            if (image.getMinDisk() > 0) {
                createSourceSize = image.getMinDisk().intValue();
            } else {
                createSourceSize = (int) Math.ceil(image.getSize() / 1024.0 / 1024.0 / 1024.0);
            }

            if (createSourceSize == 0) {
                throw new GCloudException("0010111::无法获取到镜像大�?");
            }

            Map<String, Object> propertyParams = new HashMap<>();
            propertyParams.put(ImageProperty.IMAGE_ID, params.getImageId());
            List<ImageProperty> properties = imagePropertyDao.findByProperties(propertyParams);
            Optional<ImageProperty> osTypeItem = properties.stream().filter(a -> a.getName().equals("os_type")).findFirst();
            if (osTypeItem.isPresent()) {
                platform = osTypeItem.get().getValue();
            }
        }

        if (StringUtils.isNotBlank(params.getIsoId())) {
            Iso iso = isoDao.getById(params.getIsoId());
            if (iso == null) {
                throw new GCloudException("0010124::该映像不存在");
            }
            if (createSourceSize == 0) {
                createSourceSize = (int) Math.ceil(iso.getSize() / 1024.0 / 1024.0 / 1024.0);
            }
            if (StringUtils.isBlank(platform)) {
                platform = iso.getOsType();
            }
        }

        Integer sysSize = null;

        if (params.getSystemDiskSize() == null || params.getSystemDiskSize() == 0) {
            sysSize = createSourceSize;
        } else {
            sysSize = params.getSystemDiskSize();
        }

        if (sysSize < createSourceSize) {
            throw new GCloudException("0010109::系统盘大小不能小于镜像或映像大小");
        }

        //同一个zone下，category �? storagePool �?对一
//        Map<String, StoragePool> storagePoolMap = new HashMap<>();
//
//        StoragePool pool = this.storagePoolDao.checkAndGet(params.getZoneId(), params.getSystemDiskCategory());
//        storagePoolMap.put(params.getSystemDiskCategory(), pool);

        //TODO 以后扩展�?要判断group
//        String poolHost = pool.getHostname();
//        if(StorageType.LOCAL.getValue().equals(pool.getStorageType()) && StringUtils.isBlank(poolHost)) {
//            throw new GCloudException("::存储池信息有�?");
//        }

        //TODO 磁盘类型和可用区的可用�?�校�?
        DiskCategory sysCategory = diskCategoryDao.getById(params.getSystemDiskCategory());
        if (null == sysCategory) {
            log.error("0010127::该磁盘类型不存在");
            throw new GCloudException("0010127::该磁盘类型不存在");
        }
        if (!sysCategory.isEnabled()) {
            log.error("0010128::该磁盘类型不可用");
            throw new GCloudException("0010128::该磁盘类型不可用");
        }

        //TODO 可用区的可用性校�?
        AvailableZoneEntity zone = zoneDao.getById(params.getZoneId());
        if (null == zone) {
            log.error("0010129::该可用区不存�?");
            throw new GCloudException("0010129::该可用区不存�?");
        }
        if (!zone.isEnabled()) {
            log.error("0010130::该可用区不可�?");
            throw new GCloudException("0010130::该可用区不可�?");
        }

        Map<String, DiskCategory> categoryMap = new HashMap<>();
        categoryMap.put(sysCategory.getId(), sysCategory);

        /*if((sysCategory.getMaxSize() != null && sysSize > sysCategory.getMaxSize()) || (sysCategory.getMinSize() != null && sysSize < sysCategory.getMinSize())){
            throw new GCloudException("0010110::系统盘大小不在磁盘类型允许范围内");
        }*/


        List<String> hosts = storagePoolService.categoryHost(sysCategory, zone.getId());
        if (StringUtils.isNotBlank(params.getCreateHost()) && !hosts.contains(params.getCreateHost())) {
            throw new GCloudException("0010131::�?选节点不支持�?选的磁盘类型");
        }

        if (params.getDataDisk() != null && params.getDataDisk().size() > 0) {
            volumeService.checkCategory(params.getDataDisk());

            for (DiskInfo diskInfo : params.getDataDisk()) {
                DiskCategory diskCategory = categoryMap.get(diskInfo.getCategory());
                if (diskCategory != null) {
                    continue;
                }

                diskCategory = diskCategoryDao.getById(diskInfo.getCategory());
                categoryMap.put(diskCategory.getId(), diskCategory);

                List<String> dataHosts = storagePoolService.categoryHost(diskCategory.getId(), zone.getId());

                if (StringUtils.isNotBlank(params.getCreateHost()) && !dataHosts.contains(params.getCreateHost())) {
                    throw new GCloudException("0010132::�?选节点不支持�?选的磁盘类型");
                }

                hosts.retainAll(dataHosts);

                if (hosts.size() == 0) {
                    throw new GCloudException("0010122::�?选存储池不能同时配置给云服务�?");
                }

            }
        }

        if (hosts == null || hosts.size() == 0) {
            throw new GCloudException("0010133::找不到能创建此类型磁盘的节点");
        }


        if (StringUtils.isNotBlank(params.getSubnetId())) {
            Subnet subnet = subnetDao.getById(params.getSubnetId());
            if (subnet == null) {
                throw new GCloudException("0010112::子网不存�?");
            }

            if (StringUtils.isNotBlank(params.getIpAddress())) {
                if (!NetworkUtil.checkCidrIp(subnet.getCidr(), params.getIpAddress())) {
                    throw new GCloudException("0010113::ip无效");
                }

                Map<String, Object> filters = new HashMap<>();
                filters.put(Ipallocation.IP_ADDRESS, params.getIpAddress());
                filters.put(Ipallocation.SUBNET_ID, params.getSubnetId());

                Ipallocation ipallocation = ipallocationDao.findOneByProperties(filters);
                if (ipallocation != null) {
                    throw new GCloudException("0010120::IP已经存在");
                }

            }else if(subnet.getEnableDhcp() != null && subnet.getEnableDhcp()){

                if(!portService.hasAvailableIp(subnet.getId())){
                    throw new GCloudException("0080111::没有可用的IP");
                }

            }

        }


        InstanceType instanceType = instanceTypeDao.getById(params.getInstanceType());
        if (instanceType == null) {
            throw new GCloudException("0010114::找不到计算规�?");
        }

        //TODO 对创建虚拟机的实例类型进行是否可用的判断
        if (!instanceType.isEnabled()) {
            throw new GCloudException("0010134::计算规格不可�?");
        }

        Map<String, Object> filters = new HashMap<>();
        filters.put(ZoneInstanceTypeEntity.INSTANCE_TYPE_ID, instanceType.getId());
        filters.put(ZoneInstanceTypeEntity.ZONE_ID, params.getZoneId());

        ZoneInstanceTypeEntity typeZone = zoneInstanceTypeDao.findUniqueByProperties(filters);

        if (typeZone == null) {
            throw new GCloudException("0010115::找不到该计算规格");
        }

        VmImageInfo imageCreateInfo = null;

        Integer cpu = instanceType.getVcpus();
        Integer memory = instanceType.getMemoryMb();

        VmInstance vmIns = new VmInstance();

        vmIns.setId(UUID.randomUUID().toString());
        vmIns.setImageId(params.getImageId());
        vmIns.setUserId(currentUser.getId());
        vmIns.setCreator(currentUser.getId());
        vmIns.setCore(cpu);
        vmIns.setMemory(memory);
        vmIns.setInstanceType(instanceType.getId());

        vmIns.setDisk(sysSize * 1024);

        if (imageCreateInfo != null) {
            vmIns.setImagePoolId(imageCreateInfo.getImagePoolId());
            vmIns.setImageStorageType(imageCreateInfo.getImageStorageType());
        }
        vmIns.setState(VmState.PENDING.value());
        vmIns.setLastState(VmState.PENDING.value());
        vmIns.setTaskState(VmTaskState.PENDING.value());
        vmIns.setCreateSourceId(params.getImageId());
        vmIns.setCreateType(CreateType.IMAGE.getValue());
        vmIns.setIsFt(BooleanType.FALSE.getValue());
        vmIns.setIsHa(BooleanType.FALSE.getValue());
        vmIns.setLaunchTime(new Date());
        vmIns.setAutostart(BooleanType.FALSE.getValue());
        vmIns.setUsbRedir(BooleanType.FALSE.getValue());
        vmIns.setAlias(params.getInstanceName());
        vmIns.setUseDepartment(UseDepartmentType.GCLOUD.getValue());
        vmIns.setUsbType(3);
        vmIns.setZoneId(params.getZoneId());
        vmIns.setTenantId(currentUser.getDefaultTenant());
        vmIns.setIsoId(params.getIsoId());
        vmIns.setPlatform(platform);

        String createHost = params.getCreateHost();
        Node node = null;
        if (StringUtils.isNotBlank(createHost)) {
            node = RedisNodesUtil.getComputeNodeByHostName(createHost);
            if (node == null) {
                throw new GCloudException("0010116::节点不存�?");
            }
            if (!zone.getId().equals(node.getZoneId())) {
                throw new GCloudException("0010126::�?选节点和可用区不�?");
            }
            ComputeNode computeNode = computeNodeDao.findUniqueByProperty(ComputeNode.HOSTNAME, node.getHostName());
            if (!StringUtils.equals(vmIns.getZoneId(), computeNode.getZoneId())) {
                throw new GCloudException("0010117::该可用区没有�?选的节点");
            }

            if (params.getHandleResource() != null && params.getHandleResource()) {
                //占用资源
                Dispatcher.dispatcher().assignNode(createHost, cpu, memory);
            }

        } else {
            //占用资源
            node = Dispatcher.dispatcher().assignNode(hosts, cpu, memory);
            if (node == null) {
                throw new GCloudException("0010118::节点资源不足");
            }

            createHost = node.getHostName();
        }

        StorageType storageType = null;
        try {
            StoragePool pool = storagePoolService.assignStoragePool(sysCategory, zone.getId(), createHost);
            storageType = StorageType.value(pool.getStorageType());
            StoragePoolDriver storageDriver = StoragePoolDriver.get(pool.getDriver());
            if (StringUtils.isNotBlank(params.getImageId())) {
                //暂时 volume和image的driver�?要一�?
                imageCreateInfo = VmControllerUtil.getVmImageInfo(storageDriver, storageDriver, null, params.getImageId(), null, CreateType.IMAGE, pool);
            }


        }catch (Exception ex){
            Dispatcher.dispatcher().release(createHost, cpu, memory);
            throw new GCloudException(ErrorCodeUtil.getErrorCode(ex, "0010135::获取存储池失�?"));
        }


        vmIns.setHostname(createHost);
        vmIns.setStorageType(storageType.getValue());

        try {
            vmInstanceDao.save(vmIns);
        } catch (Exception ex) {
            log.error("保存数据库失�?", ex);
            Dispatcher.dispatcher().release(createHost, cpu, memory);
            throw new GCloudException("0010119::创建云服务器系统异常");
        }


        CreateInstanceByImageInitResponse response = new CreateInstanceByImageInitResponse();
        response.setId(vmIns.getId());
        response.setCreateHost(createHost);
        response.setStorageType(storageType.getValue());
        response.setImageInfo(imageCreateInfo);
        response.setCpu(cpu);
        response.setMemory(memory);
        response.setSystemDiskSize(sysSize);
        response.setCreateUser(currentUser);
        response.setIsoId(params.getIsoId());

        return response;
    }

}