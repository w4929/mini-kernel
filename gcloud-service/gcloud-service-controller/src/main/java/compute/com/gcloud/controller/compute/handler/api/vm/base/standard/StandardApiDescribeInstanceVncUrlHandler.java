package com.gcloud.controller.compute.handler.api.vm.base.standard;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.compute.service.vm.base.IVmBaseService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.vm.base.standard.StandardApiDescribeInstanceVncUrlMsg;
import com.gcloud.header.compute.msg.api.vm.base.standard.StandardApiDescribeInstanceVncUrlReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module = Module.ECS, subModule= SubModule.VM, action = "DescribeInstanceVncUrl", versions = {ApiVersion.Standard}, name = "获取云服务器vnc")
public class StandardApiDescribeInstanceVncUrlHandler extends MessageHandler<StandardApiDescribeInstanceVncUrlMsg, StandardApiDescribeInstanceVncUrlReplyMsg>{

	@Autowired
    private IVmBaseService vmBaseService;
	
	@Override
	public StandardApiDescribeInstanceVncUrlReplyMsg handle(StandardApiDescribeInstanceVncUrlMsg msg)
			throws GCloudException {
		String url = vmBaseService.queryInstanceVNC(msg.getInstanceId());
		StandardApiDescribeInstanceVncUrlReplyMsg replyMsg = new StandardApiDescribeInstanceVncUrlReplyMsg();

        replyMsg.setVncUrl(url);
        replyMsg.setSuccess(true);
        return replyMsg;
	}

}