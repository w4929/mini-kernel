package com.gcloud.controller.compute.workflow.vm.iso;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.controller.compute.workflow.model.iso.CleanIsoFileFlowCommandReq;
import com.gcloud.controller.compute.workflow.model.iso.CleanIsoFileFlowCommandRes;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
import com.gcloud.header.compute.msg.node.vm.iso.CleanIsoConfigFileMsg;
import com.gcloud.header.compute.msg.node.vm.iso.ConfigIsoFileMsg;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Component
@Scope("prototype")
@Slf4j
public class CleanIsoFileFlowCommand extends BaseWorkFlowCommand{
	@Autowired
    private MessageBus bus;

	@Override
	protected Object process() throws Exception {
		CleanIsoFileFlowCommandReq req = (CleanIsoFileFlowCommandReq)getReqParams();

		CleanIsoConfigFileMsg msg = new CleanIsoConfigFileMsg();
        msg.setTaskId(getTaskId());
        msg.setInstanceId(req.getInstanceId());
        msg.setVmCdromDetail(req.getVmCdromDetail());
        msg.setTaskId(getTaskId());

        msg.setServiceId(MessageUtil.computeServiceId(req.getHostname()));

        bus.send(msg);
		return null;
	}

	@Override
	protected Object rollback() throws Exception {
		CleanIsoFileFlowCommandReq req = (CleanIsoFileFlowCommandReq)getReqParams();

        ConfigIsoFileMsg msg = new ConfigIsoFileMsg();
        msg.setTaskId(getTaskId());
        msg.setInstanceId(req.getInstanceId());
        msg.setVmCdromDetail(req.getVmCdromDetail());
        msg.setServiceId(MessageUtil.computeServiceId(req.getHostname()));

        bus.send(msg);
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return CleanIsoFileFlowCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return CleanIsoFileFlowCommandRes.class;
	}

}