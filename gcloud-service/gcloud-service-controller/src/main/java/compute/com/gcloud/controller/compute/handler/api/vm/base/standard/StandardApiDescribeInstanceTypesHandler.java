package com.gcloud.controller.compute.handler.api.vm.base.standard;

import com.gcloud.controller.compute.handler.api.model.DescribeInstanceTypesParams;
import com.gcloud.controller.compute.service.vm.base.IVmBaseService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.ApiUtil;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.model.InstanceTypeItemType;
import com.gcloud.header.compute.msg.api.model.standard.StandardInstanceTypeItemType;
import com.gcloud.header.compute.msg.api.vm.base.standard.StandardApiDescribeInstanceTypesMsg;
import com.gcloud.header.compute.msg.api.vm.base.standard.StandardApiDescribeInstanceTypesReplyMsg;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module = Module.ECS,subModule=SubModule.VM,action = "DescribeInstanceTypes", versions = {ApiVersion.Standard}, name = "实例类型列表")
public class StandardApiDescribeInstanceTypesHandler extends MessageHandler<StandardApiDescribeInstanceTypesMsg, StandardApiDescribeInstanceTypesReplyMsg>{

	@Autowired
	private IVmBaseService vmBaseService;
	
	@Override
	public StandardApiDescribeInstanceTypesReplyMsg handle(StandardApiDescribeInstanceTypesMsg msg) throws GCloudException {
		DescribeInstanceTypesParams params = BeanUtil.copyProperties(msg, DescribeInstanceTypesParams.class);
		PageResult<InstanceTypeItemType> response = vmBaseService.describeInstanceTypes(params);
		PageResult<StandardInstanceTypeItemType> stdResponse = ApiUtil.toPage(response, toStandardReply(response.getList()));
        
        StandardApiDescribeInstanceTypesReplyMsg replyMsg = new StandardApiDescribeInstanceTypesReplyMsg();
		replyMsg.init(stdResponse);
		return replyMsg;
	}
	
	public List<StandardInstanceTypeItemType> toStandardReply(List<InstanceTypeItemType> list) {
		if(null == list) {
			return null;
		}
		
		List<StandardInstanceTypeItemType> stdList = new ArrayList<>();
		for (InstanceTypeItemType data : list) {
			StandardInstanceTypeItemType stdData = BeanUtil.copyBean(data, StandardInstanceTypeItemType.class);
			stdData.setMemorySize(Math.ceil(stdData.getMemorySize() / 1024));
			stdList.add(stdData);
		}
		return stdList;
	}

}