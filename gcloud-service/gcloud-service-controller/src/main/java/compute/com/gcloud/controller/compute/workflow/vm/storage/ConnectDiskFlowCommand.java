package com.gcloud.controller.compute.workflow.vm.storage;

import com.gcloud.controller.compute.workflow.model.vm.ConnectDiskFlowCommandReq;
import com.gcloud.controller.compute.workflow.model.vm.ConnectDiskFlowCommandRes;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@Scope("prototype")
@Slf4j
public class ConnectDiskFlowCommand extends BaseWorkFlowCommand {

	@Override
	protected Object process() throws Exception {
		ConnectDiskFlowCommandReq req = (ConnectDiskFlowCommandReq) getReqParams();
		log.debug(String.format("èææ?%s è¿æ¥å°ç©çæº%s, å¼?å§?", req.getVolumeId(), req.getCreateHost()));

		//IVolumeService volumeService = (IVolumeService) SpringUtil.getBean("volumeServiceImpl");
		//ConnectionInfo connectionInfo = volumeService.initializeConnection(req.getVolumeId(), req.getHostName());

		log.debug(String.format("èææ?%s è¿æ¥å°ç©çæº%s, ç»æ", req.getVolumeId(), req.getCreateHost()));

		ConnectDiskFlowCommandRes res = new ConnectDiskFlowCommandRes();

		return res;
	}

	@Override
	protected Object rollback() throws Exception {
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return ConnectDiskFlowCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return ConnectDiskFlowCommandRes.class;// AttachVolumeFlowCommandRes.class;
	}
	
	@Override
	public boolean judgeExecute() {
		//ææ¶æ²¡æiscsiï¼ææ¶åæ¶?
		return false;
	}

}