package com.gcloud.controller.compute.workflow.vm.iso;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.controller.compute.ControllerComputeProp;
import com.gcloud.controller.compute.dao.InstanceDao;
import com.gcloud.controller.compute.entity.VmInstance;
import com.gcloud.controller.compute.workflow.model.iso.ConfigIsoFileFlowCommandReq;
import com.gcloud.controller.compute.workflow.model.storage.ConfigDataDiskFileFlowCommandReq;
import com.gcloud.controller.image.dao.VmIsoAttachmentDao;
import com.gcloud.controller.image.entity.VmIsoAttachment;
import com.gcloud.controller.storage.dao.StoragePoolDao;
import com.gcloud.controller.storage.dao.StoragePoolNodeDao;
import com.gcloud.controller.storage.entity.StoragePool;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
import com.gcloud.header.compute.enums.Device;
import com.gcloud.header.compute.enums.StorageType;
import com.gcloud.header.compute.enums.VmIsoAttachStatus;
import com.gcloud.header.compute.msg.node.vm.iso.ConfigIsoFileMsg;
import com.gcloud.header.compute.msg.node.vm.model.VmCdromDetail;
import com.gcloud.header.compute.msg.node.vm.storage.ConfigDataDiskFileMsg;
import com.gcloud.header.compute.msg.node.vm.storage.ForceCleanDiskConfigFileMsg;
import com.gcloud.header.storage.enums.StoragePoolDriver;
import com.gcloud.service.common.compute.uitls.VmUtil;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@Scope("prototype")
@Slf4j
public class ConfigIsoFileFlowCommand extends BaseWorkFlowCommand{
	@Autowired
    private MessageBus bus;

    @Autowired
    private InstanceDao instanceDao;
    
    @Autowired
    private VmIsoAttachmentDao vmIsoAttachmentDao;
    
    @Autowired
    private StoragePoolDao storagePoolDao;
    
    @Autowired
    private ControllerComputeProp prop;

	@Override
	protected Object process() throws Exception {
		ConfigIsoFileFlowCommandReq req = (ConfigIsoFileFlowCommandReq)getReqParams();
		
		StoragePool pool = this.storagePoolDao.getById(req.getStoragePoolId());
		
		VmIsoAttachment vmIso = new VmIsoAttachment();
		vmIso.setInstanceId(req.getInstanceId());
		vmIso.setCreatedAt(new Date());
		vmIso.setIsoId(req.getIsoId());
		vmIso.setMountpoint(Device.HDC.getValue());
		vmIso.setStatus(VmIsoAttachStatus.ATTACHING.name());
		vmIso.setIsoPool(pool.getStorageType().equals(StorageType.DISTRIBUTED.getValue())?prop.getCephIsoPool():pool.getPoolName());
		vmIso.setIsoPoolId(pool.getId());
		vmIso.setIsoStorageType(pool.getStorageType());
		
		vmIsoAttachmentDao.save(vmIso);

		VmCdromDetail vmCdromDetail = new VmCdromDetail();
        ConfigIsoFileMsg msg = new ConfigIsoFileMsg();
        msg.setTaskId(getTaskId());
        msg.setInstanceId(req.getInstanceId());
        vmCdromDetail.setIsoId(req.getIsoId());
        vmCdromDetail.setIsoPath(VmUtil.getIsoPath(req.getIsoId(), pool.getStorageType().equals(StorageType.DISTRIBUTED.getValue())?prop.getCephIsoPool():(pool.getStorageType().equals(StorageType.LOCAL.getValue())?prop.getIsoCachedPath():pool.getPoolName()), pool.getDriver()));
        vmCdromDetail.setIsoPool(pool.getStorageType().equals(StorageType.DISTRIBUTED.getValue())?prop.getCephIsoPool():pool.getPoolName());
        vmCdromDetail.setIsoPoolId(pool.getId());
        vmCdromDetail.setIsoStorageType(pool.getStorageType());
        vmCdromDetail.setIsoTargetDevice(vmIso.getMountpoint());
        msg.setVmCdromDetail(vmCdromDetail);
        msg.setPlatform(req.getPlatform());
        msg.setServiceId(MessageUtil.computeServiceId(req.getHostname()));

        bus.send(msg);
		return null;
	}

	@Override
	protected Object rollback() throws Exception {
		ConfigIsoFileFlowCommandReq req = (ConfigIsoFileFlowCommandReq)getReqParams();

        VmInstance ins = instanceDao.getById(req.getInstanceId());

        ForceCleanDiskConfigFileMsg msg = new ForceCleanDiskConfigFileMsg();
        msg.setTaskId(getTaskId());
        msg.setInstanceId(req.getInstanceId());
        msg.setVolumeId(msg.getVolumeId());

        msg.setServiceId(MessageUtil.computeServiceId(ins.getHostname()));

        bus.send(msg);
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return ConfigIsoFileFlowCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return null;
	}
    
}