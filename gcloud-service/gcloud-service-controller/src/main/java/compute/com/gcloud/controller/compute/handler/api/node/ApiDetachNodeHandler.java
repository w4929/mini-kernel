package com.gcloud.controller.compute.handler.api.node;

import com.gcloud.controller.compute.service.node.IComputeNodeService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.node.node.ApiDetachNodeMsg;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module = Module.ECS, subModule= SubModule.NODE, action = "DetachNode", name = "取消关联节点和可用区")
@GcLog(taskExpect = "取消节点关联可用�?")
public class ApiDetachNodeHandler extends MessageHandler<ApiDetachNodeMsg, ApiReplyMessage>{

	@Autowired
	private IComputeNodeService nodeService;
	
	@Override
	public ApiReplyMessage handle(ApiDetachNodeMsg msg) throws GCloudException {
		nodeService.detachNode(msg.getHostname());
		ApiReplyMessage reply = new ApiReplyMessage();
		return reply;
	}

}