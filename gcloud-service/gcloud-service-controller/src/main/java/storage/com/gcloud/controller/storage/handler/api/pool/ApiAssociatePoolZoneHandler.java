package com.gcloud.controller.storage.handler.api.pool;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.storage.model.AssociatePoolZoneParams;
import com.gcloud.controller.storage.service.IStoragePoolService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.storage.msg.api.pool.ApiAssociatePoolZoneMsg;
import com.gcloud.header.storage.msg.api.pool.ApiAssociatePoolZoneReplyMsg;
import com.gcloud.header.storage.msg.api.pool.StoragePoolActions;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@GcLog(taskExpect="存储池关联可用区")
@ApiHandler(module = Module.ECS, subModule = SubModule.DISK, action = StoragePoolActions.ASSOCIATE_POOL_ZONE, name = "存储池关联可用区")
public class ApiAssociatePoolZoneHandler extends MessageHandler<ApiAssociatePoolZoneMsg, ApiAssociatePoolZoneReplyMsg>{

	@Autowired
	private IStoragePoolService storagePoolService;
	
	@Override
	public ApiAssociatePoolZoneReplyMsg handle(ApiAssociatePoolZoneMsg msg) throws GCloudException {
		AssociatePoolZoneParams params = BeanUtil.copyProperties(msg, AssociatePoolZoneParams.class);
		storagePoolService.associatePoolZone(params, msg.getCurrentUser());
		
		ApiAssociatePoolZoneReplyMsg reply = new ApiAssociatePoolZoneReplyMsg();
		
		msg.setObjectId(msg.getPoolId());
		msg.setObjectName(CacheContainer.getInstance().getString(CacheType.STORAGEPOOL_NAME, msg.getPoolId()));
		return reply;
	}

}