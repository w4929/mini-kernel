package com.gcloud.controller.storage.service;

import java.util.List;

import com.gcloud.controller.storage.entity.Volume;
import com.gcloud.controller.storage.entity.VolumeAttachmentInfo;
import com.gcloud.controller.storage.model.CreateDiskParams;
import com.gcloud.controller.storage.model.CreateVolumeParams;
import com.gcloud.controller.storage.model.DescribeDisksParams;
import com.gcloud.controller.storage.model.DetailDiskParams;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.compute.enums.Device;
import com.gcloud.header.compute.msg.api.model.DiskInfo;
import com.gcloud.header.storage.model.DetailDisk;
import com.gcloud.header.storage.model.DiskItemType;
import com.gcloud.header.storage.model.VmVolumeDetail;
import com.gcloud.header.storage.msg.api.volume.ApiDisksStatisticsReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public interface IVolumeService {

	String create(CreateDiskParams params, CurrentUser currentUser);

	String createVolume(CreateVolumeParams params, CurrentUser currentUser);

	void deleteVolume(String id, String taskId);

	PageResult<DiskItemType> describeDisks(DescribeDisksParams params, CurrentUser currentUser);

	void updateVolume(String diskId, String diskName);
	
	void updateVolume(String diskId, String diskName, String description);

	void reserveVolume(String volumeId);

	void unreserveVolume(String volumeId);

	String attachVolume(String volumeId, String instanceUuid, String mountPoint, String hostname, String taskId);

    void beginDetachingVolume(String volumeId);

    void rollDetachingVolume(String volumeId);

	void detachVolume(String volumeId, String attachmentId);

	void resizeVolume(String diskId, Integer newSize, String taskId);

	Volume getVolume(String volumeId);

	List<VmVolumeDetail> getVolumeInfo(String instanceId);

    VmVolumeDetail volumeDetail(String volumeId, String instanceId);

    VmVolumeDetail volumeDetail(String instanceId, Device device);



	void handleDeleteVolumeSuccess(String volumeId);

    void handleDeleteVolumeFailed(String errorCode, String volumeId);

	void handleAttachVolumeSuccess(String volumeId, String attachmentId, String instanceId, String mountPoint, String hostname);

	void handleResizeVolumeSuccess(String volumeId, int newSize);

    void handleResizeVolumeFailed(String errorCode, String volumeId);

	void checkCategory(List<DiskInfo> diskInfos);

	void checkCategory(String category, Integer size);
	
	ApiDisksStatisticsReplyMsg diskStatistics(CurrentUser currentUser);
	
	DetailDisk detailDisk(DetailDiskParams params, CurrentUser currentUser);
	
	List<VolumeAttachmentInfo> getAttachmentVolume(String instanceId);
}