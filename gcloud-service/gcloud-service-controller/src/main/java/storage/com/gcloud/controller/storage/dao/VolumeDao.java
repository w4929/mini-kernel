package com.gcloud.controller.storage.dao;

import com.gcloud.common.util.ObjectUtil;
import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.storage.entity.Volume;
import com.gcloud.controller.storage.model.DescribeDisksParams;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.core.currentUser.policy.enums.UserResourceFilterPolicy;
import com.gcloud.core.currentUser.policy.model.FilterPolicyModel;
import com.gcloud.core.currentUser.policy.service.IUserResourceFilterPolicy;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.service.SpringUtil;
import com.gcloud.core.util.SqlUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.compute.enums.StorageType;
import com.gcloud.header.storage.StorageErrorCodes;
import com.gcloud.header.storage.enums.DiskType;
import com.gcloud.header.storage.enums.DiskTypeParam;
import com.gcloud.header.storage.enums.VolumeStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Repository
@Slf4j
public class VolumeDao extends JdbcBaseDaoImpl<Volume, String> {
    
    public Volume checkAndGet(String id) throws GCloudException {
        Volume volume = this.getById(id);
        if (volume == null) {
            throw new GCloudException(StorageErrorCodes.FAILED_TO_FIND_VOLUME);
        }
        return volume;
    }
 
    public boolean updateVolumeStatus(String id, VolumeStatus status){

        log.debug(String.format("==volume status== in %s volumeId=%s, status=%s", ObjectUtil.getCalledMethod(), id, status.value()));
        String sql = "update gc_volumes set status = ? where id = ?";
        Object[] values = {status.value(), id};

        return this.jdbcTemplate.update(sql, values) > 0;
    }

    public boolean updateVolumeStatus(String id, String status){

        log.debug(String.format("==volume status== in %s volumeId=%s, status=%s", ObjectUtil.getCalledMethod(), id, status));
        String sql = "update gc_volumes set status = ? where id = ?";
        Object[] values = {status, id};

        return this.jdbcTemplate.update(sql, values) > 0;
    }

    public boolean updateVolumeStatusWithProviderId(String id, String status) {
        String sql = "update gc_volumes set status = ? where provider_ref_id = ?";
        Object[] vals = {status, id};

        return this.jdbcTemplate.update(sql, vals) > 0;
    }

    public boolean syncVolume(Volume vol) {
        StringBuffer sql = new StringBuffer();
        List<Object> values = new ArrayList<>();

        sql.append("update gc_volumes set ");
        sql.append("category = ?, "); values.add(vol.getCategory());
        sql.append("description = ?, "); values.add(vol.getDescription());
        sql.append("display_name = ?, "); values.add(vol.getDisplayName());
        sql.append("size = ?, "); values.add(vol.getSize());
        sql.append("status = ?, "); values.add(vol.getStatus());
        sql.append("updated_at = ? "); values.add(vol.getUpdatedAt());
        sql.append("where id = ? and updated_at < '?'");
        values.add(vol.getId());
        values.add(vol.getUpdatedAt());

        return this.jdbcTemplate.update(sql.toString(), values.toArray()) > 0;
    }

    public <E> PageResult<E> describeDisks(DescribeDisksParams params, Class<E> clazz, CurrentUser currentUser){
    	IUserResourceFilterPolicy filterPolicy = (IUserResourceFilterPolicy)SpringUtil.getBean(UserResourceFilterPolicy.TYPICAL.getFilterPolicyClazz());
		FilterPolicyModel sqlModel = filterPolicy.filterPolicy(currentUser, "v.", ResourceIsolationCheckType.VOLUME);
		
        StringBuffer sql = new StringBuffer();
        List<Object> values = new ArrayList<>();

        //添加了sourceSnapshotId和device保证standard接口的返�?
        sql.append("select v.*, v.snapshot_id as sourceSnapshotId, aa.instance_uuid, gi.alias as instanceName, aa.mountpoint as device, gz.name as zoneName, dc.name as cnCategory, p.hostname as hostname from");
        sql.append(" gc_volumes v left join");
        sql.append(" (select a.mountpoint, a.volume_id, group_concat(a.instance_uuid) instance_uuid from gc_volume_attachments a group by a.volume_id) aa on v.id = aa.volume_id");
        sql.append(" left join gc_storage_pools p on v.pool_id=p.id");
        
        //将zoneId和instanceId相对应的名字匹配
        sql.append(" left join gc_instances as gi on aa.instance_uuid = gi.id");
        sql.append(" left join gc_zones as gz on gz.id = v.zone_id");
        sql.append(" left join gc_disk_categories as dc on dc.id = v.category");
        
        sql.append("  where 1 = 1");

        if(params != null){
        	
        	if(StringUtils.isNotBlank(params.getHostname())){
        		sql.append(" and ((p.storage_type!=? and v.zone_id = (select zone_id from gc_compute_nodes where hostname=?)) or (p.storage_type =? and p.hostname = ?))");
        		values.add(StorageType.LOCAL.getValue());
        		values.add(params.getHostname());
        		values.add(StorageType.LOCAL.getValue());
                values.add(params.getHostname());
        	}
        	
            if(StringUtils.isNotBlank(params.getDiskName())){
                sql.append(" and v.display_name like concat('%', ?, '%')");
                values.add(params.getDiskName());
            }

            if(StringUtils.isNotBlank(params.getDiskType())){
                DiskTypeParam diskType = DiskTypeParam.getByValue(params.getDiskType());
                if(diskType != null && diskType != DiskTypeParam.ALL){
                    sql.append(" and v.disk_type = ?");
                    values.add(params.getDiskType());
                }
            }

            if(StringUtils.isNotBlank(params.getStatus())){
                sql.append(" and v.status = ?");
                values.add(params.getStatus());
            }

            if(params.getStatuses() != null && params.getStatuses().size() > 0){
                sql.append(" and v.status in (").append(SqlUtil.inPreStr(params.getStatuses().size())).append(")");
                values.addAll(params.getStatuses());
            }

            if(StringUtils.isNotBlank(params.getInstanceId())){
                sql.append(" and v.id in (select va.volume_id from gc_volume_attachments va where va.instance_uuid = ?)");
                values.add(params.getInstanceId());
            }
            
            if(params.getDiskIds() != null && !params.getDiskIds().isEmpty()) {
            	sql.append(" and v.id in(").append(SqlUtil.inPreStr(params.getDiskIds().size())).append(")");
            	values.addAll(params.getDiskIds());
            }
            
            //筛�?�出有创建快照的磁盘
            if(params.isHasSnapshot()) {
            	sql.append(" and v.id in (select volume_id from gc_snapshots)");
            }

        }
        
        sql.append(sqlModel.getWhereSql());
		values.addAll(sqlModel.getParams());

        sql.append(" order by v.created_at desc");

        return findBySql(sql.toString(), values, params.getPageNumber(), params.getPageSize(), clazz);
    }


    public <E> List<E> instanceVolume(String instanceId, Class<E> clazz){

        List<Object> values = new ArrayList<>();

        StringBuffer sql = new StringBuffer();
        sql.append("select v.*, a.mountpoint from gc_volumes v, gc_volume_attachments a where v.id = a.volume_id");
        sql.append(" and a.instance_uuid = ?");
        values.add(instanceId);

        return findBySql(sql.toString(), values, clazz);
    }
    
    public <E> List<E> categoryStatistics(String beforeDate, Class<E> clazz,CurrentUser currentUser){
    	IUserResourceFilterPolicy filterPolicy = (IUserResourceFilterPolicy)SpringUtil.getBean(UserResourceFilterPolicy.TYPICAL.getFilterPolicyClazz());
		FilterPolicyModel sqlModel = filterPolicy.filterPolicy(currentUser, "gv.", ResourceIsolationCheckType.VOLUME);
		
        StringBuffer sql = new StringBuffer();
        List<Object> values = new ArrayList<>();
		sql.append("SELECT dc.id,dc.`name`,COUNT(*) as countNum ");
		sql.append("from gc_volumes gv left join gc_disk_categories dc on gv.category=dc.id ");
		sql.append("where gv.created_at < '" + beforeDate + "' ");
		 
		sql.append(sqlModel.getWhereSql());
		values.addAll(sqlModel.getParams());
			
		sql.append("GROUP BY dc.id ");
		 
	    return findBySql(sql.toString(), values, clazz);
	}
    
    public <E> List<E> statusStatistics(String beforeDate, Class<E> clazz,CurrentUser currentUser){
    	IUserResourceFilterPolicy filterPolicy = (IUserResourceFilterPolicy)SpringUtil.getBean(UserResourceFilterPolicy.TYPICAL.getFilterPolicyClazz());
		FilterPolicyModel sqlModel = filterPolicy.filterPolicy(currentUser, "gv.", ResourceIsolationCheckType.VOLUME);
		List<Object> values = new ArrayList<>();
		 StringBuffer sql = new StringBuffer();
		 sql.append("SELECT status,COUNT(*) as countNum ");
		 sql.append("from gc_volumes gv ");
		 sql.append("where gv.created_at < '" + beforeDate + "' ");
		 
		 sql.append(sqlModel.getWhereSql());
		 values.addAll(sqlModel.getParams());
			
		 sql.append("GROUP BY status ");
		 
		return findBySql(sql.toString(), values, clazz);
	}
    
    public <E> List<E> getAttachMentVolume(String instanceId, Class<E> clazz) {
    	StringBuffer sql = new StringBuffer();
    	List<Object> values = new ArrayList<>();
    	
    	sql.append("select gv.*, ");
    	sql.append(" gva.id as attachmentId, gva.mountpoint as mountPoint, gva.instance_uuid as instanceUuid, gva.attached_host as attachedHost, gva.provider as attachProvider, gva.provider_ref_id as attachProviderRefId from gc_volumes as gv ");
    	sql.append(" left join gc_volume_attachments as gva on gv.id = gva.volume_id ");
    	sql.append(" where gva.instance_uuid = ?");
    	values.add(instanceId);
    	
    	return findBySql(sql.toString(), values, clazz);
    }

    public Volume systemVolume(String instanceId){
        StringBuffer sql = new StringBuffer();
        List<Object> values = new ArrayList<>();

        sql.append("select v.* from gc_volumes v, gc_volume_attachments a");
        sql.append(" where v.id = a.volume_id and v.disk_type = ? and a.instance_uuid = ?");

        values.add(DiskType.SYSTEM.getValue());
        values.add(instanceId);

        List<Volume> volumes = findBySql(sql.toString(), values);
        return volumes != null && volumes.size() > 0 ? volumes.get(0) : null;
    }


    public boolean hasLocalVolume(String hostname){

        StringBuffer sql = new StringBuffer();
        sql.append("select count(1) from gc_volumes v where v.pool_id in");
        sql.append(" (select id from gc_storage_pools p where p.storage_type = ? and p.hostname = ?) limit 1");

        Object[] values = {StorageType.LOCAL.getValue(), hostname};

        int num = this.jdbcTemplate.queryForObject(sql.toString(), values, Integer.class);
        return num > 0;
    }

    public boolean instanceHasLocalVolume(String instanceId){

        StringBuffer sql = new StringBuffer();

        sql.append("select count(1) from gc_volumes v, gc_volume_attachments a where v.id = a.volume_id");
        sql.append(" and v.storage_type = ? and a.instance_uuid = ? limit 1");

        Object[] values = {StorageType.LOCAL.getValue(), instanceId};

        int num = this.jdbcTemplate.queryForObject(sql.toString(), values, Integer.class);
        return num > 0;
    }
}