package com.gcloud.controller.storage.handler.node.volume;

import com.gcloud.controller.storage.service.IVolumeService;
import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.header.storage.msg.node.volume.NodeDeleteDiskReplyMsg;

import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Handler
public class NodeDeleteDiskReplyHandler extends AsyncMessageHandler<NodeDeleteDiskReplyMsg> {

    @Autowired
    private IVolumeService volumeService;

    @Override
    public void handle(NodeDeleteDiskReplyMsg msg) {
        if (msg.getSuccess()) {
            this.volumeService.handleDeleteVolumeSuccess(msg.getVolumeId());
        }
        else {
            this.volumeService.handleDeleteVolumeFailed(msg.getErrorCode(), msg.getVolumeId());
        }
    }

}