package com.gcloud.controller.storage.handler.api.pool;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.storage.entity.DiskCategory;
import com.gcloud.controller.storage.model.UnassociateDiskCategoryPoolParams;
import com.gcloud.controller.storage.service.IStoragePoolService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.storage.msg.api.pool.ApiUnassociateDiskCategoryPoolMsg;
import com.gcloud.header.storage.msg.api.pool.ApiUnassociateDiskCategoryPoolReplyMsg;
import com.gcloud.header.storage.msg.api.pool.StoragePoolActions;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@GcLog(taskExpect="磁盘类型取消关联存储�?")
@ApiHandler(module = Module.ECS, subModule = SubModule.DISK, action = StoragePoolActions.UNASSOCIATE_DISK_CATEGORY_POOL, name = "磁盘类型取消关联存储�?")
public class ApiUnassociateDiskCategoryPoolHandler extends MessageHandler<ApiUnassociateDiskCategoryPoolMsg, ApiUnassociateDiskCategoryPoolReplyMsg>{

	@Autowired
    private IStoragePoolService poolService;
	
	@Override
	public ApiUnassociateDiskCategoryPoolReplyMsg handle(ApiUnassociateDiskCategoryPoolMsg msg) throws GCloudException {
		UnassociateDiskCategoryPoolParams params = BeanUtil.copyProperties(msg, UnassociateDiskCategoryPoolParams.class);
		poolService.unassociateDiskCategoryPool(params);
		
		ApiUnassociateDiskCategoryPoolReplyMsg reply = new ApiUnassociateDiskCategoryPoolReplyMsg();
		
		//添加操作日志
		DiskCategory diskCategory = poolService.getDiskCategoryById(msg.getDiskCategoryId());
		String diskCategoryName = null;
		if(diskCategory != null) {
			diskCategoryName = diskCategory.getName();
		}
		
		msg.setObjectId(msg.getDiskCategoryId());
		msg.setObjectName(diskCategoryName);
		return reply;
	}

}