package com.gcloud.controller.timer;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.log.model.LogFeedbackParams;
import com.gcloud.controller.log.service.impl.LogService;
import com.gcloud.core.handle.MessageTimeoutHandler;
import com.gcloud.core.service.SpringUtil;
import com.gcloud.header.GMessage;
import com.gcloud.header.NodeMessage;
import com.gcloud.header.log.enums.LogStatus;
import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Slf4j
public class TimeoutHandlerThread implements Runnable {

    private MessageTimeoutHandler messageTimeoutHandler;
    private GMessage message;

    public TimeoutHandlerThread() {
    }

    public TimeoutHandlerThread(MessageTimeoutHandler messageTimeoutHandler, GMessage message) {
        this.messageTimeoutHandler = messageTimeoutHandler;
        this.message = message;
    }

    @Override
    public void run() {
        messageTimeoutHandler.timeout(message);
        try{
            if(message instanceof NodeMessage && StringUtils.isNotBlank(message.getTaskId())) {

                LogService logService = SpringUtil.getBean(LogService.class);

                NodeMessage nodemsg = (NodeMessage)message;
                LogFeedbackParams params = new LogFeedbackParams();
                params.setTaskId(nodemsg.getTaskId());
                params.setStatus(LogStatus.TIMEOUT.getValue());
                params.setCode(nodemsg.getErrorCode());
                logService.feedback(params);
            }
        }catch (Exception ex){
            log.error("::feedback error", ex);
        }
    }
}