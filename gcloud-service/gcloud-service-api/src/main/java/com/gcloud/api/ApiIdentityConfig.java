package com.gcloud.api;

import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@ConfigurationProperties(prefix = "gcloud.identity.user")
public class ApiIdentityConfig {

	@Value("${gcloud.identity.user.tokenTimeout:300}")
    private int tokenTimeout;//单位s
	
	
	@Value("${gcloud.identity.user.verifyCode:false}")
    private boolean verifyCode;
	
	@Value("${gcloud.identity.api.check.signatureCheck:false}")
    private boolean signatureCheck;
	
	@Value("${gcloud.identity.api.check.timeout:5}")
    private int apiTimeout;
	
	@Value("${gcloud.identity.api.test.testMode:false}")
    private boolean testMode;//测试模式
	
	@Value("${gcloud.identity.api.test.testTenantId:}")
    private String testTenantId;
	
	@Value("${gcloud.identity.api.test.testUserId:}")
    private String testUserId;

	public String getTestTenantId() {
		return testTenantId;
	}

	public void setTestTenantId(String testTenantId) {
		this.testTenantId = testTenantId;
	}

	public String getTestUserId() {
		return testUserId;
	}

	public void setTestUserId(String testUserId) {
		this.testUserId = testUserId;
	}

	public boolean isTestMode() {
		return testMode;
	}

	public void setTestMode(boolean testMode) {
		this.testMode = testMode;
	}

	public int getTokenTimeout() {
		return tokenTimeout;
	}

	public void setTokenTimeout(int tokenTimeout) {
		this.tokenTimeout = tokenTimeout;
	}

	public boolean isVerifyCode() {
		return verifyCode;
	}

	public void setVerifyCode(boolean verifyCode) {
		this.verifyCode = verifyCode;
	}

	public boolean isSignatureCheck() {
		return signatureCheck;
	}

	public void setSignatureCheck(boolean signatureCheck) {
		this.signatureCheck = signatureCheck;
	}

	public int getApiTimeout() {
		return apiTimeout;
	}

	public void setApiTimeout(int apiTimeout) {
		this.apiTimeout = apiTimeout;
	}
}