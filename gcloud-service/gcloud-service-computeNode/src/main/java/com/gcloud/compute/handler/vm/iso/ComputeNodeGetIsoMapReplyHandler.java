package com.gcloud.compute.handler.vm.iso;

import java.util.ArrayList;
import java.util.List;

import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.header.compute.msg.node.vm.iso.ComputeNodeGetIsoMapReplyMsg;
import com.gcloud.header.compute.msg.node.vm.model.IsoMapItem;
import com.gcloud.service.common.util.RbdUtil;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Slf4j
@Handler
public class ComputeNodeGetIsoMapReplyHandler extends AsyncMessageHandler<ComputeNodeGetIsoMapReplyMsg>{
	
	@Override
	public void handle(ComputeNodeGetIsoMapReplyMsg msg) {
		log.debug("ComputeNodeGetIsoMapReplyHandler start");
		List<IsoMapItem> mappeds = RbdUtil.getRbdmaps();
		List<String> mappedStrs = new ArrayList<String>();
		for(IsoMapItem mapped:mappeds) {
			mappedStrs.add(mapped.getPoolName() + "_" + mapped.getIsoId());
		}
		for(IsoMapItem item:msg.getIsoMapItems()) {
			if(!mappedStrs.contains(item.getPoolName() + "_" + item.getIsoId())) {
				RbdUtil.rbdmap(item.getIsoId(), item.getPoolName(), "::rbd map失败");
			}
		}
	}

}