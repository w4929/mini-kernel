package com.gcloud.compute.cache.cache;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Slf4j
public class VmCpuPinCache {

    //不缓存所有，因为可能虚拟机中关机，无法处理�??
    //因为�?机又start times，为保证并发，不锁整个开机过程，记录正在�?机的虚拟机占用的cpu
    private static Map<String, List<Integer>> cpuRetain = new HashMap<>();

    public static void retain(String instanceId, List<Integer> cpuSets){
        cpuRetain.put(instanceId, cpuSets);
        log.debug("cpu pin after retain:" + JSONObject.toJSONString(cpuRetain));
    }

    public static void release(String instanceId){
        cpuRetain.remove(instanceId);
        log.debug("cpu pin after release:" + JSONObject.toJSONString(cpuRetain));
    }

    public static List<Integer> retainCpus(){
        log.debug("cpu pin get retain:" + JSONObject.toJSONString(cpuRetain));
        List<Integer> retain = new ArrayList<>();
        for(List<Integer> cpuPin : cpuRetain.values()){
            retain.addAll(cpuPin);
        }
        return retain;
    }


}