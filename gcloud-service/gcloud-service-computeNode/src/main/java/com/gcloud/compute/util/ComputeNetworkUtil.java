package com.gcloud.compute.util;

import com.gcloud.common.util.FileUtil;
import com.gcloud.common.util.SystemUtil;
import com.gcloud.compute.prop.ComputeNodeProp;
import com.gcloud.core.service.SpringUtil;
import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Slf4j
public class ComputeNetworkUtil {

    private static final String CHECK_NET_SH = "network/checkNet.sh";
    private static final String CHECK_OVS_SH = "network/checkOvs.sh";

    public static boolean isNetExist(String netName) {

        boolean isExist = false;
        ComputeNodeProp prop = SpringUtil.getBean(ComputeNodeProp.class);
        String scriptPath = prop.getConfigurePath();
        String script = FileUtil.getDirectoryString(scriptPath) + CHECK_NET_SH;

        try {

            String[] cmd = new String[] { script, netName };
            int ret = SystemUtil.runAndGetCode(cmd);
            isExist = ret == 0;

        } catch (Exception e) {
            log.error("运行�?查网络是否存在脚本失�?", e);
            isExist = false;
        }

        return isExist;

    }

    public static boolean isOvsExist(String ovsName) {

        boolean isExist = false;
        ComputeNodeProp prop = SpringUtil.getBean(ComputeNodeProp.class);
        String scriptPath = prop.getConfigurePath();
        String script = FileUtil.getDirectoryString(scriptPath) + CHECK_OVS_SH;

        try {

            int ret = 0;
            String[] cmd = new String[] { script, ovsName };
            ret = SystemUtil.runAndGetCode(cmd);
            isExist = ret == 0;

        } catch (Exception e) {
            log.error("运行�?查ovs是否存在脚本失败", e);
            isExist = false;
        }

        return isExist;

    }

}