package com.gcloud.compute.handler.vm.base;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.compute.virtual.ILdapDomainService;
import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.ErrorCodeUtil;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.compute.msg.node.vm.base.MoveDomainMsg;
import com.gcloud.header.compute.msg.node.vm.base.MoveDomainReplyMsg;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Slf4j
@Handler
public class MoveDomainHandler extends AsyncMessageHandler<MoveDomainMsg>{
	@Autowired
	private ILdapDomainService domainService;
	@Autowired
    private MessageBus bus;
	@Override
	public void handle(MoveDomainMsg msg) {
		// TODO Auto-generated method stu
		MoveDomainReplyMsg reply=new MoveDomainReplyMsg();
		boolean result=false;
		try {
			result=domainService.move(msg.getInstanceId(), msg.getOldDoamin(), msg.getOldUser(), msg.getOldPassword(), msg.getDomain(), msg.getUser(), msg.getPassword());
		}catch (Exception e) {
			// TODO: handle exception
			 log.error("::虚拟机入域失�?", e);
			 reply.setErrorCode(ErrorCodeUtil.getErrorCode(e, "::虚拟机入域失�?"));
		}
		
		reply.setDomain(msg.getDomain());
		reply.setInstanceId(msg.getInstanceId());
		reply.setSuccess(result);
        reply.setServiceId(MessageUtil.controllerServiceId());
        bus.send(reply);
	}


}