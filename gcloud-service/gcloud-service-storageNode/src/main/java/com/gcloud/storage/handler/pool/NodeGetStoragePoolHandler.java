package com.gcloud.storage.handler.pool;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.Handler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.storage.msg.node.pool.NodeGetStoragePoolMsg;
import com.gcloud.header.storage.msg.node.pool.NodeGetStoragePoolReplyMsg;
import com.gcloud.storage.service.IStoragePoolService;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Handler
public class NodeGetStoragePoolHandler extends MessageHandler<NodeGetStoragePoolMsg, NodeGetStoragePoolReplyMsg>{
	@Autowired
	IStoragePoolService storagePoolService;
	
	@Override
	public NodeGetStoragePoolReplyMsg handle(NodeGetStoragePoolMsg msg) throws GCloudException {
		NodeGetStoragePoolReplyMsg reply = new NodeGetStoragePoolReplyMsg();
		reply.setInfo(storagePoolService.getLocalPoolInfo(msg.getPoolName()));

        reply.setSuccess(true);
        return reply;
	}

}