package com.gcloud.storage.driver;

import java.util.List;

import org.springframework.stereotype.Component;

import com.gcloud.core.exception.GCloudException;
import com.gcloud.header.ResourceProviderVo;
import com.gcloud.header.storage.enums.StoragePoolDriver;
import com.gcloud.service.common.compute.uitls.DiskQemuImgUtil;
import com.gcloud.service.common.lvm.uitls.LvmUtil;
import com.gcloud.storage.NodeStoragePoolVo;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@Slf4j
public class StorageLvmDriver implements IStorageDriver{
	
	@Override
	public StoragePoolDriver driver() {
		return StoragePoolDriver.LVM;
	}

	@Override
	public void createDisk(NodeStoragePoolVo pool, String volumeId, Integer size, String imageId, String snapshotId)
			throws GCloudException {
		// lvcreate -L 2500 -n volumeName poolName
		//创建了块设备/dev/poolName/volumeName
		boolean lvCreate = LvmUtil.lvCreate(pool.getPoolName(), "volume-" + volumeId, size, "::创建块设�?" + LvmUtil.getVolumePath(pool.getPoolName(), volumeId) + "失败");
		
        if (imageId == null) {
        	LvmUtil.activeLv(LvmUtil.getVolumePath(pool.getPoolName(), volumeId));
        	//配置数据盘的大小�?100G（即云服务器里第二块盘的空间大小�?
    		//qemu-img create -f qcow2 /dev/poolName/volumeName 100G
        	DiskQemuImgUtil.create(size, "G", LvmUtil.getVolumePath(pool.getPoolName(), volumeId), "qcow2");
        } else {
        	//判断镜像在节点上是否�?�?
        	//如果不是�?活，则进行激活操�?
        	LvmUtil.activeLv(LvmUtil.getImagePath(pool.getPoolName(), imageId));
        	
        	//qemu-img create -b /dev/poolName/imageId -f qcow2 /dev/poolName/volumeName
        	DiskQemuImgUtil.create(LvmUtil.getImagePath(pool.getPoolName(), imageId), LvmUtil.getVolumePath(pool.getPoolName(), volumeId), "qcow2");
        }
		
	}

	@Override
	public void deleteDisk(NodeStoragePoolVo pool, String volumeId, List<ResourceProviderVo> snapshots)
			throws GCloudException {
		//umount /dev/poolName/volumeName -- 这步�?要吗�?
		//lvremove /dev/poolName/volumeName
		//lvdisplay | grep "/dev/poolName/volumeName"
		LvmUtil.remove(pool.getPoolName(), "volume-" + volumeId);
		
	}

	@Override
	public void resizeDisk(NodeStoragePoolVo pool, String volumeId, Integer oldSize, Integer newSize)
			throws GCloudException {
		// lvextend -L +10G /dev/poolName/volumeName
		//e2fsck /dev/poolName/volumeName  [强烈建议先不�?-f]  扫描文件信息 --这步�?要吗�?
		//resize2fs /dev/poolName/volumeName  --这步�?要吗�?
		
		//缩小�?要以下步�?
		//umount 挂载�?
		//resize2fs /dev/poolName/volumeName 2G 
		//lvreduce -L 2G /dev/poolName/volumeName
		//mount
		LvmUtil.extend(pool.getPoolName(), "volume-" + volumeId, newSize-oldSize);
		DiskQemuImgUtil.resize(LvmUtil.getVolumePath(pool.getPoolName(), volumeId), newSize-oldSize);
		
	}

	@Override
	public void createSnapshot(NodeStoragePoolVo pool, String volumeRefId, String snapshotId, String snapshotRefId)
			throws GCloudException {
		//lvcreate -s -l 100 -n volumeNameSnap /dev/poolName/volumeName
		//-s    关键选项，创建快照snap的意�?    
	    //-l    后面跟快照包含多少个PE的数�?
	    //-n    后面跟创建的快照的名�?
	    //-p r  由于快照大多为只读，改�?�项为为修改权限位只读（r�?
		
		//lvdisplay /dev/poolName/volumeNameSnap
		//LvmUtil.snap(pool.getPoolName(), "volume-" + volumeRefId, snapshot.getId());
		//改用内部快照
		DiskQemuImgUtil.snapshot(snapshotId, LvmUtil.getVolumePath(pool.getPoolName(), volumeRefId));
	}

	@Override
	public void deleteSnapshot(NodeStoragePoolVo pool, String volumeRefId, String snapshotId, String snapshotRefId)
			throws GCloudException {
		// lvremove /dev/poolName/volumeNameSnap 
		//LvmUtil.remove(pool.getPoolName(), "volume-" + volumeRefId);
		DiskQemuImgUtil.deleteSnapshot(snapshotId, LvmUtil.getVolumePath(pool.getPoolName(), volumeRefId));
	}

	@Override
	public List<String> resetSnapshot(NodeStoragePoolVo pool, String volumeRefId, String snapshotId,
			String snapshotRefId, Integer size) throws GCloudException {
		// lvconvert --merge /dev/poolName/volumeNameSnap
		//LvmUtil.convert(pool.getPoolName(), snapshot.getId());
		//恢复快照后，快照会被自动删除掉，程序�?要将数据库中对应的快照记录删除？
		
		DiskQemuImgUtil.resetSnapshot(snapshotId, LvmUtil.getVolumePath(pool.getPoolName(), volumeRefId));
		return null;
	}
	
}