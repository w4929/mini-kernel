package com.gcloud.storage.timer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.storage.msg.node.node.StorageNodeConnectMsg;
import com.gcloud.service.common.compute.uitls.VmUtil;
import com.gcloud.storage.StorageNodeProp;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@Slf4j
public class StorageNodeConnectControllerTimer {
	@Autowired
    private MessageBus bus;
	
	@Autowired
    private StorageNodeProp prop;
	
	@Scheduled(fixedDelayString = "${gcloud.storageNode.reportFrequency:30}" + "000")
    public void connect(){

        log.debug("Storage ConnectControllerTimer begin");

        try {
            StorageNodeConnectMsg msg = new StorageNodeConnectMsg();
            msg.setServiceId(MessageUtil.controllerServiceId());
            msg.setHostname(VmUtil.getHostName());
            msg.setNodeTimeout(prop.getReportFrequency() * 2);
            bus.send(msg);

        } catch (Exception e) {
            log.error("连接存储控制器失�?", e);
        }

        log.debug("Storage ConnectControllerTimer end");

    }
}